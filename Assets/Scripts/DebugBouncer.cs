﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugBouncer : MonoBehaviour {

    float bounce = 1f / 32f;
    float time;

	// Use this for initialization
	void Start () {
        time = 0f;
	}
	
	// Update is called once per frame
	void LateUpdate () {
        gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + bounce, gameObject.transform.position.z);
        time += Time.deltaTime;
        if (time > 0.3333f)
        {
            bounce *= -1f;
            time -= 0.3333f;
        }
	}
}
