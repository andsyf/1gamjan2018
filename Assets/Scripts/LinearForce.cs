﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinearForce : MonoBehaviour {

    MobilePosition mobilePosition;
    public float force;
    public Vector2 direction;

    // Use this for initialization
    void Start()
    {
        mobilePosition = gameObject.GetComponent<MobilePosition>();
        if (mobilePosition == null)
        {
            Debug.LogError("ERROR - LinearForce.cs is trying to get a MobilePosition from an object that doesn't have one");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (force > 0f)
        {
            //Apply movement
            mobilePosition.position = mobilePosition.position + ((Vector3)direction * force * Time.deltaTime);
            //Attenuate the force
            force *= 0.9f;
            //Clapm
            if(force < 0.1f)
            {
                force = 0f;
            }
        }
        else
        {
            //My work here is done
            Destroy(this);
        }
    }
}
