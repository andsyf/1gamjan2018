﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VillainController : MonoBehaviour {

    //The villain can have the following states
    // 1) Moving to position
    // 2) Offering contract
    // 3) Contract is being read
    // 4) Timing out and going home or going home after contract is accepted
    // Hence we should stop timer once Villain is clicked on, so we can add some collision logic to the raycast in this controller and it will all work out.......

    Vector3 targetPosition;
    Vector3 doorPosition;

    public float TimeToWait { set { timeToWait = value; } }
    float timeToWait;
    MobilePosition mobilePosition;
    BoxCollider2D villainCollider;

    //
    public Faction VillainFaction;

    //Edit stuff
    public GameObject ExclamationPoint;

    //Internal stuff that needs to be shared
    [HideInInspector]
    public List<Contract> ContractsToPay;

    [HideInInspector]
    public bool Waiting = false;

    [HideInInspector]
    public List<Message> DialogStream = new List<Message>();

    [HideInInspector]
    public bool PaymentDue = false;

    bool clickable = false;

    void Awake()
    {
        //Position ourselves on spawn
        //Grab some essential data
        Vector3 spawnPos = new Vector3(Random.Range(-3f, 3f), Random.Range(1f, 3f), 0f);
        gameObject.transform.position = GameDataManager.ConvertedDoorPosition + spawnPos.normalized * 15f;
        mobilePosition = gameObject.GetComponent<MobilePosition>();
        villainCollider = gameObject.GetComponent<BoxCollider2D>();
        //Wait between 10 to 15 seconds once in position
        timeToWait = Random.Range(10f, 15f);
        clickable = false;
        //Where is the door
        doorPosition = GameDataManager.ConvertedDoorPosition;
        //Where are we going
        targetPosition = PlayerProcessor.GetAnOfferPoint(this);
        ExclamationPoint.SetActive(false);
        ContractsToPay = new List<Contract>();
    }

    // Use this for initialization
    void Start()
    {

    }

    void Update()
    {
        villainCollider.enabled = (clickable && GameDataManager.Instance.ClickablesEnabled);
    }

    public void GetVillainous(float timeTillStart)
    {
        StartCoroutine(GetVillainousIn(timeTillStart));
    }

    IEnumerator GetVillainousIn(float t)
    {
        yield return new WaitForSeconds(t);
        GetVillainous();
    }

    public void GetVillainous()
    {
        //Bring back some monsters
        PlayerProcessor.ReturnMonstersForFaction(VillainFaction);

        if (targetPosition != PlayerProcessor.Instance.NoOfferPoint)
        {
            MoveToOfferSpotAndOfferContract();
        }
        else
        {
            WalkAimlesslyOutside();
        }
    }

    public void Click()
    {
        ExclamationPoint.SetActive(false);
        clickable = false;
        StopAllCoroutines();
    }

    public void GoHome()
    {
        StartCoroutine(_GoHome());
    }

    IEnumerator _GoHome()
    {
        //Don't need the offer point anymore
        PlayerProcessor.ReleaseOfferPoint(this);
        //Hide the exclamation point
        ExclamationPoint.SetActive(false);
        //Then go home
        targetPosition = GameDataManager.ConvertedVillainSpawnPoint;
        //Make myself unclickable
        clickable = false;

        //Move to the door
        while (doorPosition != mobilePosition.position)
        {
            Vector3 vecToTarget = doorPosition - mobilePosition.position;
            mobilePosition.position += vecToTarget.normalized * mobilePosition.Stats.MovementSpeed * Time.deltaTime;
            Vector3 newVecToTarget = doorPosition - mobilePosition.position;
            if (newVecToTarget.sqrMagnitude > vecToTarget.sqrMagnitude)
            {
                //We've arrived
                mobilePosition.position = doorPosition;
            }
            yield return 0;
        }

        while (targetPosition != mobilePosition.position)
        {
            Vector3 vecToTarget = targetPosition - mobilePosition.position;
            mobilePosition.position += vecToTarget.normalized * mobilePosition.Stats.MovementSpeed * Time.deltaTime;
            Vector3 newVecToTarget = targetPosition - mobilePosition.position;
            if(PaymentDue && mobilePosition.position.y > 7f)
            {
                for (int i = 0; i < ContractsToPay.Count; i++)
                {
                    PlayerProcessor.SpawnPayment(this, ContractsToPay[i]);
                }
                PaymentDue = false;
            }
            if (newVecToTarget.sqrMagnitude > vecToTarget.sqrMagnitude)
            {
                //We've arrived
                mobilePosition.position = targetPosition;
            }
            yield return 0;
        }
        //We are home, kill yourself
        //But wait for 2 seconds first
        yield return new WaitForSeconds(2f);
        ReleaseResourcesAndDestroy();
    }

    void ReleaseResourcesAndDestroy()
    {
        PlayerProcessor.Instance.ActiveVillain = false;
        Destroy(gameObject);

    }

    //Implementation
    void WalkAimlesslyOutside()
    {
        StartCoroutine(_WalkAimlesslyOutside());
    }

    IEnumerator _WalkAimlesslyOutside()
    {
        bool waiting = true;
        float checkTime = 1f;
        gameObject.AddComponent<WalkAimlesslyOutside>();
        while (waiting)
        {
            //Do we have a place we can go to yet?
            if (checkTime <= 0f)
            {
                checkTime += 1f;
                targetPosition = PlayerProcessor.GetAnOfferPoint(this);
                if (targetPosition != PlayerProcessor.Instance.NoOfferPoint)
                {
                    waiting = false;
                }
            }
            yield return 0;
            //Decrement the check timer!!!! YOU FORGOT TO DO THIS AND IT BROKE EVERYTHING
            checkTime -= Time.deltaTime;
        }
        Destroy(this.GetComponent<WalkAimlesslyOutside>());
        MoveToOfferSpotAndOfferContract();
    }

    void MoveToOfferSpotAndOfferContract()
    {
        StartCoroutine(_MoveToOfferSpotAndOfferContract());
    }

    IEnumerator _MoveToOfferSpotAndOfferContract()
    {
        //Move to the door
        while (doorPosition != mobilePosition.position)
        {
            Vector3 vecToTarget = doorPosition - mobilePosition.position;
            mobilePosition.position += vecToTarget.normalized * mobilePosition.Stats.MovementSpeed * Time.deltaTime;
            Vector3 newVecToTarget = doorPosition - mobilePosition.position;
            if (newVecToTarget.sqrMagnitude > vecToTarget.sqrMagnitude)
            {
                //We've arrived
                mobilePosition.position = doorPosition;
            }
            yield return 0;
        }

        //Move to the contract offer spot
        while (targetPosition != mobilePosition.position)
        {
            Vector3 vecToTarget = targetPosition - mobilePosition.position;
            mobilePosition.position += vecToTarget.normalized * mobilePosition.Stats.MovementSpeed * Time.deltaTime;
            Vector3 newVecToTarget = targetPosition - mobilePosition.position;
            if (newVecToTarget.sqrMagnitude > vecToTarget.sqrMagnitude)
            {
                //We've arrived
                mobilePosition.position = targetPosition;
            }
            yield return 0;
        }

        Debug.Log("VILLAIN - I'm here!");
        //We are now at contract offer spot, wait your wait time
        float curTime = 0f;
        ExclamationPoint.SetActive(true);
        //Make myself clickable
        clickable = true;

        while (curTime < timeToWait)
        {
            yield return 0;
            curTime += Time.deltaTime;
        }
        //Time is up!
        //Hide the exclamation point
        ExclamationPoint.SetActive(false);
        //Bark about going home TODO

        Debug.Log("VILLAINCONTROLLER - OK I guess you don't want to talk to me thats cool...");
        Debug.LogWarning("VILLAINCONTROLLER - TODO logic to handle making a villain unhappy");

        GoHome();
    }
}
