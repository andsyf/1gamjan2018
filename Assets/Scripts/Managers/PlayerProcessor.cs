﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerProcessor : MonoBehaviour {

    public static PlayerProcessor Instance;
    public int PlayerGold;
    public int PlayerMagic;
    public int PlayerBones;
    public int PlayerMeat;
    [HideInInspector]
    public int TotalPlayerGold;
    [HideInInspector]
    public int TotalPlayerMagic;
    [HideInInspector]
    public int TotalPlayerBones;
    [HideInInspector]
    public int TotalPlayerMeat;
    public int StableCapacity;
    public int StableCurrentUsage;
    public int RentGold;
    public int RentBones;
    public int RentMeat;
    public int RentMagic;

    public List<Monster> playerStable = new List<Monster>();
    List<Monster> stableTotal = new List<Monster>();
    Dictionary<Faction, List<Monster>> factionAssignedMonsters = new Dictionary<Faction, List<Monster>>();

    MobilePosition sun, moon;

    bool gameCycle;
    float currentTime;

    float treasureXMin, treasureYMin;
    int treasureXRange, treasureYRange;

    bool nextOfferCentral = false;

    VillainController currentVillain;
    [HideInInspector]
    //public Contract CurrentContract;
    public Message CurrentMessage;
    [HideInInspector]
    public List<Message> DialogStream = new List<Message>();
    int dialogStreamPosition;
    
    //As we implement the new villain system this returns only false in order to stop extra villains from insta suiciding
    //TODO - Remove and re-engineer this bit
    [HideInInspector]
    public bool ActiveVillain { get { return false; } set { } }
    [HideInInspector]
    public List<VillainController> villainQueue = new List<VillainController>();
    Dictionary<VillainController, Vector3> villainOfferPoints = new Dictionary<VillainController, Vector3>();

    List<Vector3> freeOfferPoints = new List<Vector3>();
    public Vector3 NoOfferPoint { get { return new Vector3(0f, 100f, 0f); } set { } }

    void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        if(Instance != this)
        {
            Destroy(Instance);
            Instance = this;
        }
    }

	// Use this for initialization
	void Start () {
        ActiveVillain = false;
        currentTime = 0f;
        gameCycle = false;
        sun = GameDataManager.Instance.Sun.GetComponent<MobilePosition>();
        moon = GameDataManager.Instance.Moon.GetComponent<MobilePosition>();

        for (int i = 0; i < GameDataManager.Instance.ContractOfferPoints.Count; i++)
        {
            freeOfferPoints.Add(GameDataManager.Instance.ContractOfferPoints[i]);
        }

        treasureXMin = -5f;
        treasureXRange = 11;
        treasureYMin = 1.5f;
        treasureYRange = 9;

        factionAssignedMonsters[Faction.DarkLord] = new List<Monster>();
        factionAssignedMonsters[Faction.DarkMonk] = new List<Monster>();
        factionAssignedMonsters[Faction.Demons] = new List<Monster>();
        factionAssignedMonsters[Faction.Medusa] = new List<Monster>();
        factionAssignedMonsters[Faction.NoFaction] = new List<Monster>();
        factionAssignedMonsters[Faction.Undead] = new List<Monster>();
        factionAssignedMonsters[Faction.Werefolk] = new List<Monster>();
        factionAssignedMonsters[Faction.Witches] = new List<Monster>();
    }

    // Update is called once per frame
    void Update () {
        if (gameCycle)
        {
            currentTime += Time.deltaTime;
            if(currentTime > GameDataManager.Instance.CycleTimeInSeconds)
            {
                currentTime -= GameDataManager.Instance.CycleTimeInSeconds;
            }
            //Position sun and moon
            Vector3 v1 = sun.position;
            Vector3 v2 = moon.position;
            float half = (GameDataManager.Instance.CycleTimeInSeconds / 2f);
            float quarter = (GameDataManager.Instance.CycleTimeInSeconds / 4f);
            float sunIndent  = 1f - (Mathf.Abs((currentTime - quarter)        / half)) * 2f;
            float moonIndent = 1f - (Mathf.Abs((currentTime - quarter - half) / half)) * 2f;
            v1.x = -6.6f + sunIndent * 1.5f;
            v2.x = -6.6f + moonIndent * 1.5f;
            sun.position = v1;
            moon.position = v2;
            //DayNightCycle calculation
            GameDataManager.Instance.DayNightTimerBar.transform.localScale = new Vector3(GameDataManager.Instance.DayNightTimerBar.transform.localScale.x, 20f * (1f - (currentTime / GameDataManager.Instance.CycleTimeInSeconds)), GameDataManager.Instance.DayNightTimerBar.transform.localScale.z);
        }
    }

    public static void DemandNotSatisfied(Faction f)
    {
        Debug.LogWarning("PLAYERPROCESSOR - Add further detail to game over processing...");
        GameFlowManager.QueueEventList("GAMEOVER");
    }

    public static void AddMonster(MonsterType mt)
    {
        Debug.LogWarning("PLAYERPROCESS - Need to adjust this to work better with new UI");
        GameUIManager.AddMonster(mt);
        Instance.AddMonster(GameDataManager.GenerateMonsterOfType(mt));
    }

    void AddMonster(Monster m)
    {
        stableTotal.Add(m);
        playerStable.Add(m);
        m.UnityObject.transform.position = GameDataManager.ConvertedVillainSpawnPoint;
        m.UnityObject.GetComponent<WalkAimlessly>().TargetPosition = GameDataManager.ConvertedDoorPosition + Vector3.down;
        //Update stats
        Debug.Log("TODO - Add logic to update capacity etc");
    }

    public static void ProcessDialogStream(VillainController vc)
    {
        if (Instance.dialogStreamPosition == 0)
        {
            //First time through ProcessDialogStream for this run
            Instance.currentVillain = vc;
            vc.Click();
        }
        //Reads the current dialog stream count and then either spawns the appropriate window or terminates
        if (Instance.dialogStreamPosition < Instance.DialogStream.Count)
        {
            Message m = Instance.DialogStream[Instance.dialogStreamPosition];
            Instance.CurrentMessage = m;
            Instance.dialogStreamPosition++;
            switch (m.Type)
            {
                case MessageType.Offer:
                    DisplayContractDialog(vc);
                    break;
                case MessageType.Speech:
                    DisplayCommunicationDialog(vc);
                    break;
                case MessageType.Demand:
                    DisplayDemandDialog(vc);
                    break;
                case MessageType.Unblock:
                    //Special case, stop blocking logic flow
                    GameLogic.Instance.Waiting = false;
                    ProcessDialogStream(vc);
                    break;
            }
        }
        else
        {
            Debug.LogWarning("PLAYERPROCESSOR - We're done, termination logic and return of control go here.");
            GameDataManager.MoveWindowOut();
            Instance.currentVillain.GoHome();
        }
    }

    public static void DisplayCommunicationDialog(VillainController vc)
    {
        GameDataManager.Instance.commsWindowController.Clear();
        GameDataManager.Instance.commsWindowController.DisplayText = Instance.CurrentMessage.MessageText;
        GameDataManager.Instance.commsWindowController.MoveIn(vc);
    }

    public static void DisplayContractDialog(VillainController vc)
    {
        Contract contract = Instance.CurrentMessage.Contract;
        GameDataManager.Instance.offerWindowController.Clear();
        //Calculate how many monsters we need
        Dictionary<MonsterType, int> requestDict = contract.RequestedMonsters;
        Dictionary<MonsterType, int> offerDict = contract.OfferedMonsters;
        List<MonsterType> requiredMonsters = new List<MonsterType>();
        List<MonsterType> offeredMonsters = new List<MonsterType>();
        foreach (MonsterType t in requestDict.Keys)
        {
            for(int i = 0; i < requestDict[t]; i++)
            {
                requiredMonsters.Add(t);
            }
        }
        foreach (MonsterType t in offerDict.Keys)
        {
            for (int i = 0; i < offerDict[t]; i++)
            {
                offeredMonsters.Add(t);
            }
        }

        for (int i = 0; i < requiredMonsters.Count; i++)
        {
            GameDataManager.Instance.offerWindowController.AddRequirement(Instantiate(GameDataManager.Instance.MonsterTypeToIcon[requiredMonsters[i]]));
        }
        for (int i = 0; i < offeredMonsters.Count; i++)
        {
            GameDataManager.Instance.offerWindowController.AddPayment(Instantiate(GameDataManager.Instance.MonsterTypeToIcon[offeredMonsters[i]]));
        }

        //Calculate request
        for (int i = 0; i < contract.RequestedGold; i++)  { GameDataManager.Instance.offerWindowController.AddRequirement(Instantiate(GameDataManager.Instance.GoldIconPrefab)); }
        for (int i = 0; i < contract.RequestedBones; i++) { GameDataManager.Instance.offerWindowController.AddRequirement(Instantiate(GameDataManager.Instance.BonesIconPrefab)); }
        for (int i = 0; i < contract.RequestedMagic; i++) { GameDataManager.Instance.offerWindowController.AddRequirement(Instantiate(GameDataManager.Instance.MagicIconPrefab)); }
        for (int i = 0; i < contract.RequestedMeat; i++)  { GameDataManager.Instance.offerWindowController.AddRequirement(Instantiate(GameDataManager.Instance.MeatIconPrefab)); }
        //Calculate offer
        for (int i = 0; i < contract.OfferedGold; i++)  { GameDataManager.Instance.offerWindowController.AddPayment(Instantiate(GameDataManager.Instance.GoldIconPrefab)); }
        for (int i = 0; i < contract.OfferedBones; i++) { GameDataManager.Instance.offerWindowController.AddPayment(Instantiate(GameDataManager.Instance.BonesIconPrefab)); }
        for (int i = 0; i < contract.OfferedMagic; i++) { GameDataManager.Instance.offerWindowController.AddPayment(Instantiate(GameDataManager.Instance.MagicIconPrefab)); }
        for (int i = 0; i < contract.OfferedMeat; i++)  { GameDataManager.Instance.offerWindowController.AddPayment(Instantiate(GameDataManager.Instance.MeatIconPrefab)); }
        
        GameDataManager.Instance.offerWindowController.MoveIn(vc);
    }

    public static void DisplayDemandDialog(VillainController vc)
    {
        {
            Contract contract = Instance.CurrentMessage.Contract;
            GameDataManager.Instance.demandWindowController.Clear();

            //Demand message
            GameDataManager.Instance.demandWindowController.DisplayText = Instance.CurrentMessage.MessageText;

            //Calculate how many monsters we need
            Dictionary<MonsterType, int> requestDict = contract.RequestedMonsters;
            List<MonsterType> requiredMonsters = new List<MonsterType>();
            foreach (MonsterType t in requestDict.Keys)
            {
                for (int i = 0; i < requestDict[t]; i++)
                {
                    requiredMonsters.Add(t);
                }
            }
            for (int i = 0; i < requiredMonsters.Count; i++)
            {
                GameDataManager.Instance.demandWindowController.AddRequirement(Instantiate(GameDataManager.Instance.MonsterTypeToIcon[requiredMonsters[i]]));
            }

            //Calculate request
            for (int i = 0; i < contract.RequestedGold; i++) { GameDataManager.Instance.demandWindowController.AddRequirement(Instantiate(GameDataManager.Instance.GoldIconPrefab)); }
            for (int i = 0; i < contract.RequestedBones; i++) { GameDataManager.Instance.demandWindowController.AddRequirement(Instantiate(GameDataManager.Instance.BonesIconPrefab)); }
            for (int i = 0; i < contract.RequestedMagic; i++) { GameDataManager.Instance.demandWindowController.AddRequirement(Instantiate(GameDataManager.Instance.MagicIconPrefab)); }
            for (int i = 0; i < contract.RequestedMeat; i++) { GameDataManager.Instance.demandWindowController.AddRequirement(Instantiate(GameDataManager.Instance.MeatIconPrefab)); }

            GameDataManager.Instance.demandWindowController.MoveIn(vc);
        }
    }

    public static bool CanSatisfyContract()
    {
        bool contractSatisfied = true;
        Dictionary<MonsterType, int> countOfStable = new Dictionary<MonsterType, int>();
        for(int i = 0; i < Instance.playerStable.Count;i++)
        {
            //Add the type
            if(!countOfStable.ContainsKey(Instance.playerStable[i].Type))
            {
                countOfStable[Instance.playerStable[i].Type] = 0;
            }
            countOfStable[Instance.playerStable[i].Type] += 1;
        }
        //Sieve for fail conditions
        //TODO replace this foreach with something that doesn't leak memory
        foreach(MonsterType m in Instance.CurrentMessage.Contract.RequestedMonsters.Keys)
        {
            if ((!countOfStable.ContainsKey(m)) || 
                  countOfStable.ContainsKey(m) && Instance.CurrentMessage.Contract.RequestedMonsters[m] > countOfStable[m])
            {
                contractSatisfied = false;
            }
        }

        //Check each resource type
        contractSatisfied &= (Instance.CurrentMessage.Contract.RequestedGold <= Instance.PlayerGold);
        contractSatisfied &= (Instance.CurrentMessage.Contract.RequestedMeat <= Instance.PlayerMeat);
        contractSatisfied &= (Instance.CurrentMessage.Contract.RequestedBones <= Instance.PlayerBones);
        contractSatisfied &= (Instance.CurrentMessage.Contract.RequestedMagic <= Instance.PlayerMagic);
        return contractSatisfied;
    }

    public static void ReturnMonstersForFaction(Faction faction)
    {
        List<Monster> returnees = Instance.factionAssignedMonsters[faction];
        for(int i = 0; i < returnees.Count; i++)
        {
            Destroy(returnees[i].UnityObject.GetComponent<VentureForth>());
            returnees[i].UnityObject.SendMessage("StartAI");
        }
    }

    public static void AcceptCurrentOffer()
    {
        //Check nothing has changed whether we can accept the contract in the meantime, if so then automatically refuse
        if(Instance.CurrentMessage.ActuallyExecute && CanSatisfyContract())
        {
            List<Monster> contractors = new List<Monster>();
            List<Monster> newStable = new List<Monster>();
            //Duplicate the dictionary
            Dictionary<MonsterType, int> searchees = Instance.CurrentMessage.Contract.RequestedMonsters;
            for(int i = 0; i < Instance.playerStable.Count; i++)
            {
                if(searchees.ContainsKey(Instance.playerStable[i].Type) && searchees[Instance.playerStable[i].Type] > 0)
                {
                    contractors.Add(Instance.playerStable[i]);
                    searchees[Instance.playerStable[i].Type]--;
                }
                else
                {
                    newStable.Add(Instance.playerStable[i]);
                }
            }
            Instance.playerStable = newStable;
            //Update UI
            
            //Tell all the monsters to go home
            for(int i = 0; i < contractors.Count; i++)
            {
                contractors[i].UnityObject.SendMessage("StopAI");
                contractors[i].UnityObject.AddComponent<VentureForth>();
                Instance.factionAssignedMonsters[Instance.currentVillain.VillainFaction].Add(contractors[i]);
            }

            //Deduct resources
            for (int i = 0; i < Instance.CurrentMessage.Contract.RequestedGold;  i++) { DecrementResource(ResourceType.Gold); }
            for (int i = 0; i < Instance.CurrentMessage.Contract.RequestedMeat;  i++) { DecrementResource(ResourceType.Meat); }
            for (int i = 0; i < Instance.CurrentMessage.Contract.RequestedBones; i++) { DecrementResource(ResourceType.Bones); }
            for (int i = 0; i < Instance.CurrentMessage.Contract.RequestedMagic; i++) { DecrementResource(ResourceType.Magic); }
            Instance.currentVillain.PaymentDue = true;
            Instance.currentVillain.ContractsToPay.Add(new Contract(Instance.CurrentMessage.Contract));
        }
        else
        {
            if(Instance.CurrentMessage.ActuallyExecute && Instance.CurrentMessage.Type == MessageType.Demand)
            {
                //We are unable to satisfy a demand
                PlayerProcessor.DemandNotSatisfied(Instance.currentVillain.VillainFaction);
            }
        }
        //Whatever happens, clear the window
        if(Instance.CurrentMessage.Type == MessageType.Offer)
            GameDataManager.Instance.offerWindowController.Clear();
        if(Instance.CurrentMessage.Type == MessageType.Demand)
            GameDataManager.Instance.demandWindowController.Clear();
    }

    public static void RefuseCurrentOffer()
    {
        PlayerProcessor.DamageFaction(Instance.currentVillain);
        //Whatever happens, clear the window
        GameDataManager.Instance.offerWindowController.Clear();
    }

    public static void DismissDialogWindow() {
        switch(Instance.CurrentMessage.Type)
        {
            case MessageType.Offer:
                GameDataManager.Instance.offerWindowController.MoveOut();
                break;
            case MessageType.Speech:
                GameDataManager.Instance.commsWindowController.MoveOut();
                break;
            case MessageType.Demand:
                GameDataManager.Instance.demandWindowController.MoveOut();
                break;
            default:
                break;
        }
        ProcessDialogStream(Instance.currentVillain);
    }

    public static void DamageFaction(VillainController vc)
    {
        GameDataManager.Instance.FactionScore[vc.VillainFaction]--;
        GameObject g = Instantiate(GameDataManager.Instance.BrokenHeart);
        g.transform.position = vc.transform.position + Vector3.up;
    }

    public static void AddFaction(VillainController vc)
    {
        GameDataManager.Instance.FactionScore[vc.VillainFaction]++;
    }

    public static void LoadDialogStream(List<Message> newDialogStream)
    {
        if (newDialogStream.Count > 0)
        {
            Instance.DialogStream = newDialogStream;
            Instance.dialogStreamPosition = 0;
        }
    }

    public static void SpawnPayment(VillainController vc, Contract c)
    {
        //Spawn payment in random directions at the villain point
        Vector3 vp = vc.gameObject.transform.position;
        for (int i = 0; i < c.OfferedBones; i++) { GameObject g = GameDataManager.GenerateResourceOfType(ResourceType.Bones); g.name = "Bones"; g.transform.position = vp; LinearForceToTarget l = g.AddComponent<LinearForceToTarget>(); l.target = new Vector2(Instance.treasureXMin + (float)Random.Range(0, Instance.treasureXRange), Instance.treasureYMin + (float)Random.Range(0, Instance.treasureYRange)); }
        for (int i = 0; i < c.OfferedGold; i++) { GameObject g = GameDataManager.GenerateResourceOfType(ResourceType.Gold); g.name = "Gold"; g.transform.position = vp; LinearForceToTarget l = g.AddComponent<LinearForceToTarget>(); l.target = new Vector2(Instance.treasureXMin + (float)Random.Range(0, Instance.treasureXRange), Instance.treasureYMin + (float)Random.Range(0, Instance.treasureYRange)); }
        for (int i = 0; i < c.OfferedMeat; i++) { GameObject g = GameDataManager.GenerateResourceOfType(ResourceType.Meat); g.name = "Meat"; g.transform.position = vp; LinearForceToTarget l = g.AddComponent<LinearForceToTarget>(); l.target = new Vector2(Instance.treasureXMin + (float)Random.Range(0, Instance.treasureXRange), Instance.treasureYMin + (float)Random.Range(0, Instance.treasureYRange)); }
        for (int i = 0; i < c.OfferedMagic; i++) { GameObject g = GameDataManager.GenerateResourceOfType(ResourceType.Magic); g.name = "Magic"; g.transform.position = vp; LinearForceToTarget l = g.AddComponent<LinearForceToTarget>(); l.target = new Vector2(Instance.treasureXMin + (float)Random.Range(0, Instance.treasureXRange), Instance.treasureYMin + (float)Random.Range(0, Instance.treasureYRange)); }
        //Unroll the dictionary
        Dictionary<MonsterType, int> offeredMonsters = c.OfferedMonsters;
        foreach (MonsterType k in offeredMonsters.Keys)
        {
            for (int i = 0; i < offeredMonsters[k]; i++)
            {
                AddMonster(k);
            }
        }
    }            

    public static void StartGameCycle()
    {
        Instance.gameCycle = true;
    }

    public static void StopGameCycle()
    {
        Instance.gameCycle = false;
    }

    public static void IncrementResource(ResourceType rt)
    {
        switch(rt)
        {
            case ResourceType.Gold:
                Instance.PlayerGold++;
                Instance.TotalPlayerGold++;
                break;
            case ResourceType.Bones:
                Instance.PlayerBones++;
                Instance.TotalPlayerBones++;
                break;
            case ResourceType.Magic:
                Instance.PlayerMagic++;
                Instance.TotalPlayerMagic++;
                break;
            case ResourceType.Meat:
                Instance.PlayerMeat++;
                Instance.TotalPlayerMeat++;
                break;
            default:
                break;
        }
    }

    public static void DecrementResource(ResourceType rt, int number)
    {
        for(int i = 0; i < number; i++)
        {
            DecrementResource(rt);
        }
    }

    public static void DecrementResource(ResourceType rt)
    {
        switch (rt)
        {
            case ResourceType.Gold:
                if (Instance.PlayerGold > 0) {
                    Instance.PlayerGold--;
                }
                break;
            case ResourceType.Bones:
                if (Instance.PlayerBones > 0) {
                    Instance.PlayerBones--;
                }
                break;
            case ResourceType.Magic:
                if (Instance.PlayerMagic > 0) {
                    Instance.PlayerMagic--;
                }
                break;
            case ResourceType.Meat:
                if (Instance.PlayerMeat > 0) {
                    Instance.PlayerMeat--;
                }
                break;
            default:
                break;
        }
    }

    public static Vector3 GetAnOfferPoint(VillainController vc)
    {
        //Hacky BS
        if(Instance.nextOfferCentral)
        {
            Instance.nextOfferCentral = false;
            return GameDataManager.Instance.CentralOfferPoint.transform.position;
        }

        if(Instance.villainOfferPoints.ContainsKey(vc))
        {
            return Instance.villainOfferPoints[vc];
        }
        Vector3 offerPoint = Vector3.zero;
        if(Instance.freeOfferPoints.Count > 0)
        {
            int randomIndex = Random.Range(0, Instance.freeOfferPoints.Count);
            offerPoint = Instance.freeOfferPoints[randomIndex];
            Instance.freeOfferPoints.RemoveAt(randomIndex);
            Instance.villainOfferPoints[vc] = offerPoint;
        }
        else
        {
            offerPoint = Instance.NoOfferPoint;
        }
        return offerPoint;
    }

    public static void ReleaseOfferPoint(VillainController vc)
    {
        if(Instance.villainOfferPoints.ContainsKey(vc))
        {
            Instance.freeOfferPoints.Add(Instance.villainOfferPoints[vc]);
            Instance.villainOfferPoints.Remove(vc);
        }
    }

    public static void PayRent()
    {
        //Moving away from the explicit day night cycle
        /*
        bool canPayRent = true;
        canPayRent &= (Instance.PlayerGold >= Instance.RentGold);
        canPayRent &= (Instance.PlayerMeat >= Instance.RentMeat);
        canPayRent &= (Instance.PlayerBones >= Instance.RentBones);
        canPayRent &= (Instance.PlayerMagic >= Instance.RentMagic);
        if(canPayRent)
        {
            for (int i = 0; i < Instance.PlayerGold; i++)  { PlayerProcessor.DecrementResource(ResourceType.Gold); }
            for (int i = 0; i < Instance.PlayerMeat; i++)  { PlayerProcessor.DecrementResource(ResourceType.Meat); }
            for (int i = 0; i < Instance.PlayerBones; i++) { PlayerProcessor.DecrementResource(ResourceType.Bones); }
            for (int i = 0; i < Instance.PlayerMagic; i++) { PlayerProcessor.DecrementResource(ResourceType.Magic); }
        }
        else
        {
            Debug.LogWarning("PLAYERPROCESSOR - GameOver");
        }*/
    }

    public static void PayResources()
    {
        //Moving away from the explicit day night cycle
        /*
        Monster m;
        for (int i = 0; i < Instance.stableTotal.Count; i++)
        {
            m = Instance.stableTotal[i];
            if (m.Gold > 0)  {
                //Check we have enough to pay
                if(m.Gold > Instance.PlayerGold)
                {
                    //We don't
                }
                DecrementResource(ResourceType.Gold,  m.Gold); }
            if (m.Bones > 0)
            {                 //Check we have enough to pay
                if (m.Bones > Instance.PlayerBones)
                {
                    //We don't
                }
                DecrementResource(ResourceType.Bones, m.Gold);  }
            if (m.Magic > 0)
            {                 //Check we have enough to pay
                if (m.Magic > Instance.PlayerMagic)
                {
                    //We don't
                }
                DecrementResource(ResourceType.Magic, m.Gold); }
            if (m.Meat > 0)
            {                 //Check we have enough to pay
                if (m.Meat > Instance.PlayerMeat)
                {
                    //We don't
                }
                DecrementResource(ResourceType.Meat,  m.Gold); }
        }
        */
    }

    public static void NextOfferPointIsCentral()
    {
        Instance.nextOfferCentral = true;
    }
}
 