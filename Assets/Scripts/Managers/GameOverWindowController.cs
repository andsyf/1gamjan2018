﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverWindowController : MonoBehaviour {

    public GameObject GameOverWindowTop;
    public GameObject GameOverWindowBottom;
    public GameObject GameOverWindowMidPrefab;
    public GameObject ExitButton;
    public GameObject ExitButtonDown;
    public GameObject Background;

    List<GameObject> gameOverWindowMids = new List<GameObject>();
    List<GameOverElementTextMeshProcessor> gameOverWindowDisplayElements = new List<GameOverElementTextMeshProcessor>();
    List<string> gameOverReports = new List<string>();

    ConstantMoveToTarget constantMoveToTarget;

    // Use this for initialization
    void Start () {

    }

	// Update is called once per frame
	void LateUpdate () {
		if(ExitButton.transform.position.y >= 0f)
        {
            ExitButton.transform.position = Vector3.zero;
            Background.transform.position = Vector3.zero;
        }
        if (ExitButtonDown.transform.position.y >= 0f)
        {
            ExitButtonDown.transform.position = Vector3.zero;
            Background.transform.position = Vector3.zero;
        }
    }

    public void ShowGameOverWindow()
    {
        constantMoveToTarget = gameObject.GetComponent<ConstantMoveToTarget>();
        constantMoveToTarget.TargetPosition = Vector3.up * 2f * 50f;

        LoadGOString("Programming\nAndrew Syfret");
        LoadGOString("");
        LoadGOString("Cheerleading\nGraham Weldon");
        LoadGOString("");
        LoadGOString("Testing\nBenjamin Moran");
        LoadGOString("");
        LoadGOString("Coding Mentoring\nBernhard");
        LoadGOString("");
        LoadGOString("Social API Dev\nJohn Babich");
        LoadGOString("");
        LoadGOString("Inspired by the\nworks of\nCome-Come-Cat");
        LoadGOString("");
        LoadGOString("Thank YOU\nFor Playing!");
        LoadGOString("");

        ConstructGameOverWindow();
    }

    public void LoadGOString(string s)
    {
        gameOverReports.Add(s);
    }

    public void ConstructGameOverWindow()
    {
        int size = gameOverReports.Count;
        for (int i = 0; i < size; i++)
        {
            GameObject g =Instantiate(GameOverWindowMidPrefab);
            g.transform.parent = gameObject.transform;
            g.transform.localPosition = Vector3.up * 1.5f + Vector3.down * i;
            gameOverWindowMids.Add(g);
            gameOverWindowDisplayElements.Add(gameOverWindowMids[gameOverWindowMids.Count - 1].GetComponent<GameOverElementTextMeshProcessor>());
        }
        GameOverWindowBottom.transform.localPosition = Vector3.up * 1.5f + Vector3.down * (size - 1) + Vector3.up * 0.25f;
        for (int i = 0; i < size; i++)
        {
            gameOverWindowDisplayElements[i].DisplayText = gameOverReports[i];
        }
    }
}
