﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour {

    public static MenuManager Instance;
    public List<GameObject> MenuOptions = new List<GameObject>();

    public GameObject MenuScreen;
    public GameObject PressAnyKeyToStart;

    public List<GameObject> GameUIElements = new List<GameObject>();

    void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        if(Instance != this)
        {
            Destroy(Instance);
            Instance = this;
        }
    }

	// Use this for initialization
	void Start () {
        Debug.Log("TODO - Logic to arrange the menu items");
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public static void DisplayTitleScreen()
    {
        Instance.MenuScreen.SetActive(true);
        Instance.PressAnyKeyToStart.SetActive(true);
    }
    public static void HideTitleScreen()
    {
        Instance.MenuScreen.SetActive(false);
        Instance.PressAnyKeyToStart.SetActive(false);
    }
    public static void DisplayGameUIElements()
    {
        for(int i = 0; i < Instance.GameUIElements.Count; i++)
        {
            Instance.GameUIElements[i].SetActive(true);
        }
    }
}
