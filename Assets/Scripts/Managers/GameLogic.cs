﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLogic : MonoBehaviour {

    public FactionData Witches;

    public int CostOfSkeleton, CostOfLizardman, CostOfDarkKnight, CostOfGhost;
    Dictionary<MonsterType, int> typeToCost = new Dictionary<MonsterType, int>();

    public static GameLogic Instance;

    int escalationLevel;

    bool gamePlaying = false;

    //Debug data
    Message a1v1, a1v2, a1v3, a2v1, a2v2, a2v3;
    Message a1min;

    [HideInInspector]
    List<Message> InitialMessage;
    [HideInInspector]
    List<Message> FirstDemand;

    [HideInInspector]
    public bool Waiting;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        if (Instance != this)
        {
            Destroy(Instance);
            Instance = this;
        }
        InitialMessage = new List<Message>();
        
        /*
        InitialMessage.Add(new Message("Minion! I demand\nthe following\ntribute from ye.", GameDataManager.GenerateContract(10, 0, 0, 0, 0, 0, 0, 0)));
        */
        ///*
        InitialMessage.Add(new Message("Minion! I command\nthee to run my\nmonster stable.\nI will return\nevery day with\nmore monsters..."));
        InitialMessage.Add(new Message("AND IF YOU CANNOT\nPAY MY TRIBUTE YOU\nWILL BE FIRED!\n"));
        InitialMessage.Add(new Message("Here are thy first\nmonsters. Use them\nwell."));
        InitialMessage.Add(new Message(GameDataManager.GenerateContract(6, 0, 0, 0, new MonsterType[] { },
                                                                        0, 0, 0, 0, new MonsterType[] { MonsterType.Skeleton, MonsterType.LizardMan, MonsterType.DarkKnight })));
        InitialMessage.Add(new Message("When I return you\nmust pay me the \nfollowing!", GameDataManager.GenerateContract(10, 0, 0, 0, 0, 0, 0, 0), false));
        InitialMessage.Add(new Message(MessageType.Unblock));
        //*/

        FirstDemand = new List<Message>();
        FirstDemand.Add(new Message("Pay your tribute!", GameDataManager.GenerateContract(10, 0, 0, 0, 0, 0, 0, 0)));
    }

    // Use this for initialization
    void Start() {
        Waiting = false;
        escalationLevel = 1;
        typeToCost[MonsterType.Skeleton] = CostOfSkeleton;
        typeToCost[MonsterType.DarkKnight] = CostOfDarkKnight;
        typeToCost[MonsterType.Ghost] = CostOfGhost;
        typeToCost[MonsterType.LizardMan] = CostOfLizardman;
    }

    // Update is called once per frame
    void Update() {

    }

    public static void Tutorial()
    {

    }

    public static void StartGame()
    {
        //Set initial values
        GameDataManager.Instance.FactionScore[Faction.DarkMonk] = 10;
        GameDataManager.Instance.FactionScore[Faction.DarkLord] = 0;
        GameDataManager.Instance.FactionScore[Faction.Demons] = 0;
        GameDataManager.Instance.FactionScore[Faction.Medusa] = 0;
        GameDataManager.Instance.FactionScore[Faction.Undead] = 0;
        GameDataManager.Instance.FactionScore[Faction.Werefolk] = 0;
        GameDataManager.Instance.FactionScore[Faction.Witches] = 0;

        Instance.escalationLevel = 1;

        //Let's go!
        Instance.gamePlaying = true;

        //Debug Data
        Instance.a1v1 = new Message(GameDataManager.GenerateContract(0, 0, 0, 0, new MonsterType[] { MonsterType.DarkKnight },
                                                                     4, 0, 0, 0, new MonsterType[] { }));
        Instance.a1v2 = new Message(GameDataManager.GenerateContract(0, 0, 0, 0, new MonsterType[] { MonsterType.Skeleton },
                                                                    10, 0, 0, 0, new MonsterType[] { }));
        Instance.a1v3 = new Message(GameDataManager.GenerateContract(0, 0, 0, 0, new MonsterType[] { MonsterType.LizardMan },
                                                                     0, 0, 1, 0, new MonsterType[] { }));
        Instance.a2v1 = new Message(GameDataManager.GenerateContract(0, 0, 0, 0, new MonsterType[] { MonsterType.DarkKnight },
                                                                     4, 0, 0, 0, new MonsterType[] { }));
        Instance.a2v2 = new Message(GameDataManager.GenerateContract(0, 0, 0, 0, new MonsterType[] { MonsterType.Skeleton },
                                                                    10, 0, 0, 0, new MonsterType[] { }));
        Instance.a2v3 = new Message(GameDataManager.GenerateContract(0, 0, 0, 0, new MonsterType[] { MonsterType.LizardMan, MonsterType.LizardMan },
                                                                     0, 0, 2, 0, new MonsterType[] { }));
        Instance.StartCoroutine(Instance._GameFlow());
    }

    IEnumerator _GameFlow()
    {
        float increment = GameDataManager.Instance.GameEventTimeIncrement;

        //Initial monster offer
        GameDataManager.SpawnMonsterOfferer(Instance.InitialMessage);
        //GameDataManager.SpawnMonsterOfferer(RandomMonsterOffer());        

        Waiting = true;
        //Block here
        while(Waiting) { yield return 0; }

        yield return new WaitForSeconds(5f);

        GameDataManager.SpawnVillain(Faction.Witches, new List<Message>() { Instance.a1v1 });
        GameDataManager.SpawnVillain(Faction.Undead, new List<Message>() { Instance.a1v2 });
        GameDataManager.SpawnVillain(Faction.Medusa, new List<Message>() { Instance.a1v3 });

        yield return new WaitForSeconds(increment);

        List<Message> MerchantDialog = new List<Message>();
        MerchantDialog.Add(new Message("So you're the new\nmonster handler.\nLet me sell you\nwhat you need."));
        MerchantDialog.Add(new Message(GameDataManager.GenerateContract( 6, 0, 0, 0, 0, 6, 0, 0)));
        MerchantDialog.Add(new Message(GameDataManager.GenerateContract( 0, 0, 1, 0, 2, 0, 0, 0)));
        MerchantDialog.Add(new Message(GameDataManager.GenerateContract(10, 0, 0, 0, 0, 0, 0, 1)));
        GameDataManager.SpawnMerchant(MerchantDialog);

        yield return new WaitForSeconds(increment);

        GameDataManager.SpawnVillain(Faction.Witches, new List<Message>() { Instance.a2v1 });
        GameDataManager.SpawnVillain(Faction.Undead, new List<Message>() { Instance.a2v2 });
        GameDataManager.SpawnVillain(Faction.Medusa, new List<Message>() { Instance.a2v3 });

        yield return new WaitForSeconds(increment);

        //TODO MonsterOfferer returns with demand
        GameDataManager.SpawnMonsterOfferer(Instance.FirstDemand);
    }

    List<Message> RandomMonsterOffer()
    {
        List<Message> nextOffer = new List<Message>();
        List<MonsterType> spawnOptions = new List<MonsterType>();
        spawnOptions.Add(MonsterType.DarkKnight);
        spawnOptions.Add(MonsterType.LizardMan);
        spawnOptions.Add(MonsterType.Skeleton);
        List<MonsterType> generatedSet = new List<MonsterType>();
        int spawnCount = escalationLevel * Random.Range(2, 4) + Random.Range(-1, 2);
        int costOfOffer = 0;
        if (spawnCount < 1)
            spawnCount = 1;
        for(int i = 0; i < spawnCount; i++)
        {
            generatedSet.Add(spawnOptions[Random.Range(0, spawnOptions.Count)]);
            costOfOffer += typeToCost[generatedSet[generatedSet.Count - 1]];
        }
        Contract c = GameDataManager.GenerateContract(costOfOffer, 0, 0, 0, new MonsterType[0], 0, 0, 0, 0, generatedSet.ToArray());
        nextOffer.Add(new Message(c));
        return nextOffer;
    }
}

public static class FactionInitializers
{
    public static void Witches(FactionData w)
    {
        FactionState Intro = new FactionState();
        FactionState One = new FactionState();
        FactionState Two = new FactionState();
        FactionState Three = new FactionState();
        FactionState Outro = new FactionState();

        List<Message> constructedMessage = new List<Message>();
        constructedMessage.Add(new Message("Greetings!\nI am Elayne, of\nthe Coven.\nWe value our\nprivacy for our\nrituals and want\nto hire guards.\n"));
        constructedMessage.Add(new Message("We prefer those\nthat cannot see,\nand have no souls.\n"));
        constructedMessage.Add(new Message("As a token of\nfriendship I offer\nyou the ghost of\nthe last person\nwho displeased us.", GameDataManager.GenerateContract(0, 0, 0, 0, new MonsterType[0], 0, 0, 0, 0, new MonsterType[] { MonsterType.Ghost }), true));

        Intro.Introduction = constructedMessage;
        Intro.NextStateAccept = One;

        constructedMessage = new List<Message>();
        constructedMessage.Add(new Message("Greetings!\nI need some guards.\nDon't worry, I'll\nbring them back.!\nAlso I would like\nto see my old\nghost, once more."));
        constructedMessage.Add(new Message(GameDataManager.GenerateContract(0, 0, 0, 0, new MonsterType[] { MonsterType.Skeleton, MonsterType.Skeleton, MonsterType.Skeleton, MonsterType.Ghost },
                                                                           10, 0, 0, 4, new MonsterType[] { })));
        One.Introduction = constructedMessage;
        One.NextStateAccept = Two;
        One.NextStateRefuse = One;

        constructedMessage = new List<Message>();
        constructedMessage.Add(new Message("Greetings again!\nI'm out of money\nbut we need more\nguards. I'll make\nthem myself!\n"));
        constructedMessage.Add(new Message("Can we have some bones?\nI'll pay you back\nlater.\n"));
        constructedMessage.Add(new Message(GameDataManager.GenerateContract(0, 0, 10, 0, new MonsterType[] { },
                                                                            0, 0,  0, 0, new MonsterType[] { })));
        Two.Introduction = constructedMessage;
        Two.NextStateAccept = Three;
        Two.NextStateRefuse = Two;

        constructedMessage = new List<Message>();
        constructedMessage.Add(new Message("I brought your\nbones back! We\nused some of them\nto make some\nguards tho..."));
        constructedMessage.Add(new Message("We need a\nsacrifice, alive.\nDon't expect\nthese to come\nback."));
        constructedMessage.Add(new Message(GameDataManager.GenerateContract(0, 0, 0, 0, new MonsterType[] { MonsterType.LizardMan, MonsterType.LizardMan, MonsterType.LizardMan, MonsterType.LizardMan, MonsterType.LizardMan },
                                                                           10, 0, 5, 0, new MonsterType[] { })));
        Three.Introduction = constructedMessage;
        Three.NextStateAccept = Outro;
        Three.NextStateRefuse = Three;

        constructedMessage = new List<Message>();
        constructedMessage.Add(new Message("Thanks for all\nyour help. We\nbrought you a gift\nto show our\ngratitude."));
        constructedMessage.Add(new Message("We'll need you to\nkeep sending us\nguards! Can't let\nanyone see us in\nthe moonlight...\n"));

        Outro.Introduction = constructedMessage;

        w.FactionStates.Add(Intro);
        w.FactionStates.Add(One);
        w.FactionStates.Add(Two);
        w.FactionStates.Add(Three);
        w.FactionStates.Add(Outro);
        w.InitialState = w.FactionStates[0];
    }
}