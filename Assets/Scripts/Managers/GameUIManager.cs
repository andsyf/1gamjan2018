﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUIManager : MonoBehaviour {

    public static GameUIManager Instance;

    public GameObject StableStack;
    public GameObject SkeletonStableIcon;
    public GameObject LizardmanStableIcon;
    public GameObject GhostStableIcon;
    public GameObject DarkKnightStableIcon;

    //Out internal list of what is where
    List<MonsterType> iconListTypes = new List<MonsterType>();
    List<GameObject> icons = new List<GameObject>();
    List<ConstantMoveToTarget> movers = new List<ConstantMoveToTarget>();
    Dictionary<MonsterType, GameObject> monsterTypeToIconMap = new Dictionary<MonsterType, GameObject>();

    void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        if(Instance != this)
        {
            Destroy(Instance);
            Instance = this;
        }
    }

	// Use this for initialization
	void Start () {
        monsterTypeToIconMap[MonsterType.Skeleton]  = SkeletonStableIcon;
        monsterTypeToIconMap[MonsterType.LizardMan] = LizardmanStableIcon;
        monsterTypeToIconMap[MonsterType.Ghost] = GhostStableIcon;
        monsterTypeToIconMap[MonsterType.DarkKnight] = DarkKnightStableIcon;
    }
    
    public static void AddMonster(MonsterType mt)
    {
        //No error checking because I -want- it to error out at this point
        GameObject g = Instantiate(Instance.monsterTypeToIconMap[mt]);
        //Find the index to add at
        int indexToAddNewIconAt = Instance.iconListTypes.Count;
        for (int i = Instance.iconListTypes.Count - 1; i >= 0; i--)
        {
            //Look for first instance and stop there
            if (Instance.iconListTypes[i] == mt)
            {
                //Set our index and quit the loop
                indexToAddNewIconAt = i + 1; i = -1;
            }
        }
        Vector3 tp = Instance.StableStack.transform.position + (new Vector3(0f, 0.8f, 0f) * indexToAddNewIconAt) + new Vector3(2f, 0f, 0f);
        g.transform.position = tp;

        Instance.icons.Insert(indexToAddNewIconAt, g);
        Instance.iconListTypes.Insert(indexToAddNewIconAt, mt);
        Instance.movers.Insert(indexToAddNewIconAt, g.GetComponent<ConstantMoveToTarget>());

        //Reassign positions based on added icon
        Instance.AssignIconPositions();
    }

    public static void RemoveMonster(MonsterType mt)
    {
        //Find first monster with this tag, move on
        for(int i = Instance.iconListTypes.Count - 1; i >= 0; i--)
        {
            if(Instance.iconListTypes[i] == mt)
            {
                //Remove logic
                Instance.icons[i].GetComponent<ConstantMoveToTarget>().TargetPosition = Instance.icons[i].GetComponent<ConstantMoveToTarget>().TargetPosition + Vector3.right * 2f;
                SeeYa seeYa = Instance.icons[i].AddComponent<SeeYa>();
                seeYa.TimeToLive = 2f;

                //Update our lists
                Instance.icons.RemoveAt(i);
                Instance.iconListTypes.RemoveAt(i);
                Instance.movers.RemoveAt(i);

                //Re-assign positions after list removal
                Instance.AssignIconPositions();

                //End the loop
                i = -1;
            }
        }
    }

    void AssignIconPositions()
    {
        for(int i = 0; i < Instance.movers.Count; i++)
        {
            Instance.movers[i].TargetPosition = Instance.StableStack.transform.position + (new Vector3(0f, 0.8f, 0f) * i);
        }
    }
}
