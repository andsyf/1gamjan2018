﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemandWindowController : MonoBehaviour {

    public GameObject WindowElements;
    public GameObject CommsWindowElements;
    public GameObject OfferWindowElements;
    public GameObject OKButton, OKButtonDown;
    public GameObject SpeakerPosition;
    public TextMesh Speech;
    public TextMesh SpeechOV;
    public TextMesh FailureToSatisfyDemand;
    public string DisplayText { get { return loadedText; } set { loadedText = value; textOrdinal = 0; characterAdditionTimer = 0f; float calcLimit = 2f / loadedText.Length; if (calcLimit > 0.1f) { calcLimit = 0.1f; } if (calcLimit < 0.033f) { calcLimit = 0.033f; } characterAdditionTimerLimit = calcLimit; } }
    float characterAdditionTimerLimit;
    float characterAdditionTimer;
    string loadedText;
    int textOrdinal;

    [HideInInspector]
    public List<GameObject> Requirements = new List<GameObject>();

    SpriteRenderer speakerSprite;
    SpriteRenderer okButtonRend, okButtonDownRend;
    BoxCollider2D okButtonCollider;
    MobilePosition mobilePosition;
    ConstantMoveToTarget mover;
    bool queueEnableButtons = false;

    void Awake()
    {
        speakerSprite = SpeakerPosition.GetComponent<SpriteRenderer>();
        okButtonRend = OKButton.GetComponent<SpriteRenderer>();
        okButtonDownRend = OKButtonDown.GetComponent<SpriteRenderer>();
        mover = GetComponent<ConstantMoveToTarget>();
        loadedText = "";
    }

	// Use this for initialization
	void Start () {
        mobilePosition = gameObject.GetComponent<MobilePosition>();
        okButtonCollider = OKButton.GetComponent<BoxCollider2D>();
        okButtonCollider.enabled = false;
        WindowElements.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
        if (queueEnableButtons)
        {
            if (mobilePosition.position == mover.TargetPosition)
            {
                EnableButtons();
                queueEnableButtons = false;
            }
        }
        if (loadedText != "" && textOrdinal < loadedText.Length)
        {
            characterAdditionTimer += Time.deltaTime;
            if (characterAdditionTimer >= characterAdditionTimerLimit)
            {
                characterAdditionTimer -= characterAdditionTimerLimit;
                textOrdinal += 1;
                Speech.text = loadedText.Substring(0, textOrdinal);
                SpeechOV.text = loadedText.Substring(0, textOrdinal);
            }
        }
        //Reset the buttons to be up, late update input function will reset it if necessary
        okButtonRend.enabled = true;
        okButtonDownRend.enabled = false;
    }

    public void SetButtonDown(string buttonName)
    {
        switch (buttonName)
        {
            case "OK":
                okButtonDownRend.enabled = true;
                okButtonRend.enabled = false;
                break;
            default:
                break;
        }
    }

    public void MoveIn(VillainController vc)
    {
        WindowElements.SetActive(true);
        OfferWindowElements.SetActive(false);
        CommsWindowElements.SetActive(false);
        speakerSprite.sprite = vc.GetComponent<SpriteRenderer>().sprite;
        GameDataManager.DisableClickables();
        mover.TargetPosition = Vector3.zero;
        OKButton.SetActive(true);

        //Can we satisfy this demand?
        if(PlayerProcessor.Instance.CurrentMessage.ActuallyExecute && !PlayerProcessor.CanSatisfyContract())
        {
            //Oh no we can't
            FailureToSatisfyDemand.text = "(You cannot pay\nthis price.)";
        }

        //Assign the positions of requirements and payment
        float xBase = -1f;
        float reqInc = 2f / (float)Requirements.Count;
        if (reqInc > 2f / 6f) { reqInc = 2f / 6f; }
        for (int i = 0; i < Requirements.Count; i++)
        {
            Requirements[i].transform.localPosition = new Vector3(xBase + i * reqInc, -0.8f, 0f);
            Requirements[i].GetComponent<SpriteRenderer>().sortingOrder = 5 + i;
        }
        queueEnableButtons = true;
    }

    public void MoveOut()
    {
        DisableButtons();
    }

    public void Clear()
    {
        for(int i = 0; i < Requirements.Count; i++)
        {
            Destroy(Requirements[i]);
        }
        Requirements = new List<GameObject>();
    }

    public void AddRequirement(GameObject g)
    {
        Requirements.Add(g);
        g.transform.parent = gameObject.transform;
    }

    public void EnableButtons()
    {
        okButtonCollider.enabled = true;
    }

    public void DisableButtons()
    {
        okButtonCollider.enabled = false;
    }
}
 