﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputProcessor : MonoBehaviour {

    public static InputProcessor Instance;
    public delegate void InputDelegate();
    InputDelegate ProcessInput;
    
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        if (Instance != this)
        {
            Destroy(Instance);
            Instance = this;
        }
    }

    // Use this for initialization
    void Start()
    {
        ProcessInput = NoInput;
    }

    //Late Update is called once per frame
    void LateUpdate()
    {
        //DEBUG
        DebugCommands();
        //END DEBUG
        ProcessInput();
    }

    public static void SetInputMode(GameState gs)
    {
        switch (gs)
        {
            case GameState.Menu:
                Instance.ProcessInput = Instance.MenuInput;
                break;
            case GameState.GamePlay:
                Instance.ProcessInput = Instance.GamePlayInput;
                break;
            case GameState.InputOff:
                Instance.ProcessInput = Instance.NoInput;
                break;
            default:
                Instance.ProcessInput = Instance.GamePlayInput;
                break;
        }
    }

    void DebugCommands()
    {
        
        if (Input.GetKeyDown(KeyCode.A))
        {
            //Add a skeleton
            PlayerProcessor.AddMonster(MonsterType.Skeleton);
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            //Add a skeleton
            PlayerProcessor.AddMonster(MonsterType.LizardMan);
        }
        if (Input.GetKeyDown(KeyCode.F))
        {
            GameUIManager.RemoveMonster(MonsterType.Skeleton);
        }
        if (Input.GetKeyDown(KeyCode.G))
        {
            GameUIManager.RemoveMonster(MonsterType.LizardMan);
        }
    }

    void GamePlayInput()
    {

        //Maturing mouse stuff, almost completed maybe ...
        if (Input.GetMouseButton(0))
        {
            Vector2 mouseDownWorldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D mouseDownHit = Physics2D.Raycast(mouseDownWorldPoint, Vector2.zero);
            if (mouseDownHit.collider != null)
            {
                if (mouseDownHit.collider.gameObject.name == "Yes")
                {
                    GameDataManager.SetOfferWindowButtonDown("Yes");
                }
                else if (mouseDownHit.collider.gameObject.name == "No")
                {
                    GameDataManager.SetOfferWindowButtonDown("No");
                }
                else if (mouseDownHit.collider.gameObject.name == "OK")
                {
                    switch (PlayerProcessor.Instance.CurrentMessage.Type)
                    {
                        case MessageType.Speech:
                            GameDataManager.SetCommsWindowButtonDown("OK");
                            break;
                        case MessageType.Demand:
                            GameDataManager.SetDemandWindowButtonDown("OK");
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            Vector2 worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(worldPoint, Vector2.zero);
            if (hit.collider != null)
            {
                VillainController vc = hit.collider.gameObject.GetComponent<VillainController>();
                if(vc != null)
                {
                    PlayerProcessor.LoadDialogStream(vc.DialogStream);
                    PlayerProcessor.ProcessDialogStream(vc);
                }
                CollectibleClickable cc = hit.collider.gameObject.GetComponent<CollectibleClickable>();
                if (cc != null)
                {
                    cc.Click();
                }

                if (hit.collider.gameObject.name == "Yes")
                {
                    PlayerProcessor.AcceptCurrentOffer();
                    PlayerProcessor.DismissDialogWindow();
                }
                else if(hit.collider.gameObject.name == "No")
                {
                    PlayerProcessor.RefuseCurrentOffer();
                    PlayerProcessor.DismissDialogWindow();
                }
                else if (hit.collider.gameObject.name == "OK")
                {
                    if(PlayerProcessor.Instance.CurrentMessage.Type == MessageType.Demand)
                    {
                        PlayerProcessor.AcceptCurrentOffer();
                    }
                    PlayerProcessor.DismissDialogWindow();
                }
            }
        }
    }

    void MenuInput()
    {
        if(Input.anyKeyDown)
        {
            GameFlowManager.QueueEventList("STARTGAME");
        }
    }

    void NoInput() { }
}