﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OGAMEvent {
    //Event is switched on to decide processing
    public string Event;
    //Data is interpreted contextually
    public string Data;

    //Constructors
    public OGAMEvent(string e)
    {
        _init(e, "");
    }
    public OGAMEvent(string e, string d)
    {
        _init(e, d);
    }
    public OGAMEvent(OGAMEvent copy)
    {
        _init(copy.Event, copy.Data);
    }
    void _init(string e, string d)
    {
        this.Event = e;
        this.Data = d;
    }
}

public enum GameState { Menu, GamePlay, InputOff }

public class GameFlowManager : MonoBehaviour {

    public static GameFlowManager Instance;

    //Queue processing variables
    Dictionary<string, List<OGAMEvent>> eventLists = new Dictionary<string, List<OGAMEvent>>();
    Queue<OGAMEvent> queue = new Queue<OGAMEvent>();
    bool eventProcessing;

    void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        if(Instance != this)
        {
            Destroy(Instance);
            Instance = this;
        }
    }

	// Use this for initialization
	void Start () {
        #region EventListDefinitions
        eventLists["INITIALLOADTOMENU"] = new List<OGAMEvent>();
        //Game loading goes here
        eventLists["INITIALLOADTOMENU"].Add(new OGAMEvent("INPUTMODE", "Menu"));
        eventLists["INITIALLOADTOMENU"].Add(new OGAMEvent("DISPLAYTITLESCREEN"));

        eventLists["STARTGAME"] = new List<OGAMEvent>();
        eventLists["STARTGAME"].Add(new OGAMEvent("HIDETITLESCREEN"));
        eventLists["STARTGAME"].Add(new OGAMEvent("DISPLAYGAMEUI"));
        eventLists["STARTGAME"].Add(new OGAMEvent("INPUTMODE", "GamePlay"));
        eventLists["STARTGAME"].Add(new OGAMEvent("ENABLECLICKABLES"));
        eventLists["STARTGAME"].Add(new OGAMEvent("TUTORIALCHECK"));
        eventLists["STARTGAME"].Add(new OGAMEvent("STARTGAME"));

        eventLists["TUTORIAL"] = new List<OGAMEvent>();
        eventLists["TUTORIAL"].Add(new OGAMEvent("SpawnTutorial"));

        eventLists["GAMEOVER"] = new List<OGAMEvent>();
        eventLists["GAMEOVER"].Add(new OGAMEvent("INPUTMODE", "Disabled"));
        eventLists["GAMEOVER"].Add(new OGAMEvent("GAMEOVER"));
        #endregion

        eventProcessing = true;
        
        //Game logic start here
        QueueEventList("INITIALLOADTOMENU");
    }
	
	// Update is called once per frame
	void Update () {
		while(eventProcessing && queue.Count > 0)
        {
            ConsumeEvent(queue.Dequeue());
        }
	}

    void ConsumeEvent(OGAMEvent e)
    {
        switch (e.Event)
        {
            case "GAMEOVER":
                PlayerProcessor.StopGameCycle();
                GameDataManager.Instance.gameOverWindowController.ShowGameOverWindow();
                break;
            case "SpawnTutorial":
                break;
            case "PayRent":
                PlayerProcessor.PayRent();
                break;
            case "PayResources":
                PlayerProcessor.PayResources();
                break;
            case "ENABLECLICKABLES":
                GameDataManager.Instance.ClickablesEnabled = true;
                break;
            case "DISABLECLICKABLES":
                GameDataManager.Instance.ClickablesEnabled = true;
                break;
            case "STARTGAME":
                GameLogic.StartGame();
                PlayerProcessor.StartGameCycle();
                break;
            case "STOPCYCLE":
                PlayerProcessor.StopGameCycle();
                break;
            case "QUEUE":
                if(eventLists.ContainsKey(e.Data))
                {
                    Debug.Log("Queueing next turn");
                    QueueEventList(e.Data);
                }
                else
                {
                    Debug.LogError("ERROR - Non-existent event list referenced");
                }
                break;
            case "DISPLAYGAMEUI":
                MenuManager.DisplayGameUIElements();
                break;
            case "DISPLAYTITLESCREEN":
                MenuManager.DisplayTitleScreen();
                break;
            case "HIDETITLESCREEN":
                MenuManager.HideTitleScreen();
                break;
            case "WAITFORSIGNAL":
                eventProcessing = false;
                break;
            case "WAITFORTIME":
                float time = float.Parse(e.Data);
                StartCoroutine(ResumeProcessingIn(time));
                break;
            case "WAITONEFRAME":
                StartCoroutine(ResumeProcessingNextFrame());
                break;
            case "INPUTMODE":
                switch(e.Data)
                {
                    case "Menu":
                        InputProcessor.SetInputMode(GameState.Menu);
                        break;
                    case "GamePlay":
                        InputProcessor.SetInputMode(GameState.GamePlay);
                        break;
                    case "Disabled":
                        InputProcessor.SetInputMode(GameState.InputOff);
                        break;
                    default:
                        InputProcessor.SetInputMode(GameState.GamePlay);
                        break;
                }
                break;
            default:
                Debug.LogWarning("WARNING - Event was not captured: " + e.Event + ", " + e.Data);
                break;
        }
    }

    public static void QueueEventList(string elName, bool jumpToFront = false) {
        if(Instance.eventLists.ContainsKey(elName))
        {
            List<OGAMEvent> l = Instance.eventLists[elName];
            for (int i = 0; i < l.Count; i++)
            {
                Instance.queue.Enqueue(new OGAMEvent(l[i]));
            }
        }
    }

    IEnumerator ResumeProcessingIn(float time)
    {
        eventProcessing = false;
        yield return new WaitForSeconds(time);
        eventProcessing = true;
    }

    IEnumerator ResumeProcessingNextFrame()
    {
        eventProcessing = false;
        yield return 0;
        eventProcessing = true;
    }

    //API
    public static void EndTurn()
    {
        //Resolve all jobs, quests and defense
        Debug.Log("TODO - Resolve all jobs, quests and defense");

        //Generate new events
        Debug.Log("TODO - Generate new events");

        //Offer events to player
        Debug.Log("TODO - Trigger turn");
    }

    public static void ResumeProcessing()
    {
        Instance.eventProcessing = true;
    }
}
