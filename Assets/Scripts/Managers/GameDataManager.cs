﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MonsterType {   Skeleton = 0,   LizardMan = 1,  Ghost = 2,      DarkKnight = 3 }
public enum ResourceType {  Gold = 0,       Magic = 1,      Bones = 2,      Meat = 3 }
public enum Faction {       NoFaction = 0,  DarkMonk = 1,   Witches = 2,    DarkLord = 3,       Medusa = 4,     Werefolk = 5,   Demons = 6,     Undead = 7 }
public enum MessageType {   Offer = 0,      Demand = 1,     Speech = 2,     Unblock = 3 }

public class Message
{
    public MessageType Type;
    public string MessageText;
    public Contract Contract;
    public bool ActuallyExecute;
    //currently unused, next iteration of branching conversations
    /*
    public int NextMessageIndex;
    public int NextMessageOnRefuse;
    public int NextMessageOnAccept;*/

    #region Constructors
    public Message()
    {
        Type = MessageType.Offer;
        MessageText = "";
        Contract = null;
        ActuallyExecute = true;
    }
    public Message(string s)
    {
        Type = MessageType.Speech;
        MessageText = s;
        Contract = null;
        ActuallyExecute = true;
    }
    public Message(Contract c) {
        Type = MessageType.Offer;
        MessageText = "";
        Contract = new Contract(c);
        ActuallyExecute = true;
    }
    public Message(string s, Contract c, bool ae = true)
    {
        Type = MessageType.Demand;
        MessageText = s;
        Contract = new Contract(c);
        ActuallyExecute = ae;
    }
    public Message(MessageType type)
    {
        Type = type;
        MessageText = "";
        Contract = null;
        ActuallyExecute = true;
    }
    public Message(Message m)
    {
        Type = m.Type;
        MessageText = m.MessageText;
        Contract = new Contract(m.Contract);
        ActuallyExecute = true;
    }
    #endregion
}

public class Monster
{
    public MonsterType Type;
    public GameObject UnityObject;
    public int Gold;
    public int Magic;
    public int Bones;
    public int Meat;
    public int Happiness;
}

public class Contract
{
    public int OfferedGold;
    public int RequestedGold;
    public int OfferedMagic;
    public int RequestedMagic;
    public int OfferedBones;
    public int RequestedBones;
    public int OfferedMeat;
    public int RequestedMeat;
    public Dictionary<MonsterType, int> OfferedMonsters;
    public Dictionary<MonsterType, int> RequestedMonsters;
    public Contract()
    {
        OfferedGold = OfferedMagic = OfferedBones = OfferedMeat = 0;
        RequestedGold = RequestedMagic = RequestedBones = RequestedMeat = 0;
        OfferedMonsters = new Dictionary<MonsterType, int>();
        RequestedMonsters = new Dictionary<MonsterType, int>();
    }
    public Contract(Contract c)
    {
        OfferedGold = c.OfferedGold; OfferedMagic = c.OfferedMagic; OfferedBones = c.OfferedBones; OfferedMeat = c.OfferedMeat;
        RequestedGold = c.RequestedGold; RequestedMagic = c.RequestedMagic; RequestedBones = c.RequestedBones; RequestedMeat = c.RequestedMeat;
        OfferedMonsters = new Dictionary<MonsterType, int>(c.OfferedMonsters);
        RequestedMonsters = new Dictionary<MonsterType, int>(c.RequestedMonsters);
    }

}

public class GameDataManager : MonoBehaviour {

    [Header("Game Settings")]
    public float CycleTimeInSeconds;
    public float GameEventTimeIncrement;

    [Header("Monsters")]
    public GameObject SkeletonPrefab;
    public GameObject LizardmanPrefab;
    public GameObject GhostPrefab;
    public GameObject DarkKnightPrefab;
    [Header("Items")]
    public GameObject GoldPrefab; public GameObject BonesPrefab; public GameObject MeatPrefab; public GameObject MagicPrefab;
    [Header("Icons")]
    public GameObject GoldIconPrefab; public GameObject BonesIconPrefab; public GameObject MeatIconPrefab; public GameObject MagicIconPrefab;
    public GameObject SkeletonIconPrefab;
    public GameObject LizardmanIconPrefab;
    public GameObject GhostIconPrefab;
    public GameObject DarkKnightIconPrefab;
    public GameObject HeartIcon;
    public GameObject Sun, Moon;
    [Header("Game Logic")]
    public OfferWindowController offerWindowController;
    public CommunicationWindowController commsWindowController;
    public DemandWindowController demandWindowController;
    public GameOverWindowController gameOverWindowController;
    public AnimationCurve LinearForceToTargetCurve;

    public GameObject DarkMonkVillain;
    public GameObject WitchVillain;
    public GameObject WerefolkVillain;
    public GameObject DemonVillain;
    public GameObject UndeadVillain;
    public GameObject MedusaVillain;
    public GameObject DarkLordVillain;

    //Neutrals
    public GameObject MonsterOfferer;
    public GameObject Merchant;
    public GameObject Dog;
    public GameObject OldLady;

    [Header("UI Elements")]
    public GameObject GoldUIIcon;
    public GameObject GoldText;
    public GameObject BonesUIIcon;
    public GameObject BonesText;
    public GameObject MagicUIIcon;
    public GameObject MagicText;
    public GameObject MeatUIIcon;
    public GameObject MeatText;
    public GameObject DayNightTimerBar;
    public GameObject StableStack;
    public GameObject Heart;
    public GameObject BrokenHeart;

    TextMesh goldText;
    TextMesh bonesText;
    TextMesh magicText;
    TextMesh meatText;

    public static GameDataManager Instance;
    public Vector2 VillainSpawnPoint;
    public List<Vector2> ContractOfferPoints = new List<Vector2>();
    public GameObject CentralOfferPoint;
    public GameObject Door;

    //Internal game vars
    [HideInInspector]
    public static Vector3 ConvertedVillainSpawnPoint { get { return Instance.VillainSpawnPoint; } }
    [HideInInspector]
    public static Vector3 ConvertedDoorPosition { get { return Instance.Door.transform.position; } }
    [HideInInspector]
    public bool ClickablesEnabled;
    [HideInInspector]
    public Dictionary<MonsterType, GameObject> MonsterTypeToIcon = new Dictionary<MonsterType, GameObject>();
    [HideInInspector]
    public Dictionary<MonsterType, GameObject> MonsterTypeToPrefab = new Dictionary<MonsterType, GameObject>();
    [HideInInspector]
    public Dictionary<Faction, int> FactionScore = new Dictionary<Faction, int>();
    [HideInInspector]
    Dictionary<Faction, GameObject> FactionVillain = new Dictionary<Faction, GameObject>();

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        if (Instance != this)
        {
            Destroy(Instance);
            Instance = this;
        }

        //Icon dictonary set up
        MonsterTypeToIcon[MonsterType.Skeleton] = GameDataManager.Instance.SkeletonIconPrefab;
        MonsterTypeToIcon[MonsterType.LizardMan] = GameDataManager.Instance.LizardmanIconPrefab;
        MonsterTypeToIcon[MonsterType.DarkKnight] = GameDataManager.Instance.DarkKnightIconPrefab;
        MonsterTypeToIcon[MonsterType.Ghost] = GameDataManager.Instance.GhostIconPrefab;

        MonsterTypeToPrefab[MonsterType.Skeleton] = GameDataManager.Instance.SkeletonPrefab;
        MonsterTypeToPrefab[MonsterType.LizardMan] = GameDataManager.Instance.LizardmanPrefab;
        MonsterTypeToPrefab[MonsterType.DarkKnight] = GameDataManager.Instance.DarkKnightPrefab;
        MonsterTypeToPrefab[MonsterType.Ghost] = GameDataManager.Instance.GhostPrefab;

        FactionScore[Faction.NoFaction] = 0;
        FactionScore[Faction.DarkMonk] = 0;
        FactionScore[Faction.DarkLord] = 0;
        FactionScore[Faction.Demons] = 0;
        FactionScore[Faction.Medusa] = 0;
        FactionScore[Faction.Undead] = 0;
        FactionScore[Faction.Werefolk] = 0;
        FactionScore[Faction.Witches] = 0;

        FactionVillain[Faction.DarkMonk] = DarkMonkVillain;
        FactionVillain[Faction.DarkLord] = DarkLordVillain;
        FactionVillain[Faction.Demons] = DemonVillain;
        FactionVillain[Faction.Medusa] = MedusaVillain;
        FactionVillain[Faction.Undead] = UndeadVillain;
        FactionVillain[Faction.Werefolk] = WerefolkVillain;
        FactionVillain[Faction.Witches] = WitchVillain;
    }

    // Use this for initialization
    void Start () {
        goldText = GoldText.GetComponent<TextMesh>();
        bonesText = BonesText.GetComponent<TextMesh>();
        magicText = MagicText.GetComponent<TextMesh>();
        meatText = MeatText.GetComponent<TextMesh>();
        ClickablesEnabled = false;
    }

    // Update is called once per frame
    void Update() {
        //Update the UI elements
        int currentUIGold = int.Parse(goldText.text);
        int currentUIBones = int.Parse(bonesText.text);
        int currentUIMagic = int.Parse(magicText.text);
        int currentUIMeat = int.Parse(meatText.text);
        if (currentUIGold > PlayerProcessor.Instance.PlayerGold) { goldText.text = (currentUIGold - 1).ToString(); }
        if (currentUIGold < PlayerProcessor.Instance.PlayerGold) { if (currentUIGold < 999) { goldText.text = (currentUIGold + 1).ToString(); } else { goldText.text = "999"; } }
        if (currentUIBones > PlayerProcessor.Instance.PlayerBones) { bonesText.text = (currentUIBones - 1).ToString(); }
        if (currentUIBones < PlayerProcessor.Instance.PlayerBones) { if (currentUIBones < 999) { bonesText.text = (currentUIBones + 1).ToString(); } else { bonesText.text = "999"; } }
        if (currentUIMagic > PlayerProcessor.Instance.PlayerMagic) { magicText.text = (currentUIMagic - 1).ToString(); }
        if (currentUIMagic < PlayerProcessor.Instance.PlayerMagic) { if (currentUIMagic < 999) { magicText.text = (currentUIMagic + 1).ToString(); } else { magicText.text = "999"; } } 
        if (currentUIMeat > PlayerProcessor.Instance.PlayerMeat) { meatText.text = (currentUIMeat - 1).ToString(); }
        if (currentUIMeat < PlayerProcessor.Instance.PlayerMeat) { if (currentUIMeat < 999) { meatText.text = (currentUIMeat + 1).ToString(); } else { meatText.text = "999"; } }
    }

    public static Contract GenerateContract(int rg, int rme, int rb, int rma, int og, int ome, int ob, int oma)
    {
        return GenerateContract(rg, rme, rb, rma, new MonsterType[0], og, ome, ob, oma, new MonsterType[0]);
    }

    public static Contract GenerateContract(int rg, int rme, int rb, int rma, List<MonsterType> rMTypes, int og, int ome, int ob, int oma, List<MonsterType> oMTypes)
    {
        return GenerateContract(og, ome, ob, oma, oMTypes.ToArray(), rg, rme, rb, rma, rMTypes.ToArray());
    }

    public static Contract GenerateContract(int rg, int rme, int rb, int rma, MonsterType[] rMTypes, int og, int ome, int ob, int oma, MonsterType[] oMTypes)
    {
        Contract c = new Contract();
        c.OfferedGold = og;
        c.OfferedMeat = ome;
        c.OfferedBones = ob;
        c.OfferedMagic = oma;
        c.RequestedGold = rg;
        c.RequestedMeat = rme;
        c.RequestedBones = rb;
        c.RequestedMagic = rma;
        for (int i = 0; i < rMTypes.Length; i++)
        {
            if (!c.RequestedMonsters.ContainsKey(rMTypes[i]))
            {
                c.RequestedMonsters[rMTypes[i]] = 0;
            }
            c.RequestedMonsters[rMTypes[i]] += 1;
        }
        for (int i = 0; i < oMTypes.Length; i++)
        {
            if (!c.OfferedMonsters.ContainsKey(oMTypes[i]))
            {
                c.OfferedMonsters[oMTypes[i]] = 0;
            }
            c.OfferedMonsters[oMTypes[i]] += 1;
        }
        return c;
    }

    public static Monster GenerateMonsterOfType(MonsterType mt)
    {
        Monster m = new Monster();
        m.UnityObject = Instantiate(Instance.MonsterTypeToPrefab[mt]);
        MobilePosition mp = m.UnityObject.GetComponent<MobilePosition>();
        m.Gold = mp.Stats.GoldPerDay;
        m.Meat = mp.Stats.MeatPerDay;
        m.Bones = mp.Stats.BonesPerDay;
        m.Magic = mp.Stats.MagicPerDay;
        m.Type = mt;
        return m;
    }

    public static GameObject GenerateResourceOfType(ResourceType rt)
    {
        GameObject g = null;
        switch(rt)
        {
            case ResourceType.Gold:
                g = Instantiate(Instance.GoldPrefab);
                break;
            case ResourceType.Bones:
                g = Instantiate(Instance.BonesPrefab);
                break;
            case ResourceType.Meat:
                g = Instantiate(Instance.MeatPrefab);
                break;
            case ResourceType.Magic:
                g = Instantiate(Instance.MagicPrefab);
                break;
            default:
                break;
        }
        return g;
    }

    public static void SpawnVillain(Faction f, List<Message> dialog)
    {
        //Select a villain to spawn
        GameObject v = Instantiate(Instance.FactionVillain[f]);
        //Villains own logic should talk over once it is placed
        VillainController vc = v.GetComponent<VillainController>();
        vc.DialogStream = dialog;
        vc.GetVillainous(Random.Range(0f, 1f)); 
    }

    public static void SpawnMonsterOfferer(List<Message> dialogStream)
    {
        PlayerProcessor.NextOfferPointIsCentral();
        GameObject g = Instantiate(Instance.MonsterOfferer);
        //Overwrite random spawn point
        g.transform.position = GameDataManager.ConvertedVillainSpawnPoint;
        VillainController vc = g.GetComponent<VillainController>();
        vc.DialogStream = dialogStream;
        vc.GetVillainous();
    }
    
    public static void SpawnMerchant(List<Message> dialogStream)
    {
        PlayerProcessor.NextOfferPointIsCentral();
        GameObject g = Instantiate(Instance.Merchant);
        //Overwrite random spawn point
        g.transform.position = GameDataManager.ConvertedVillainSpawnPoint;
        VillainController vc = g.GetComponent<VillainController>();
        vc.DialogStream = dialogStream;
        vc.GetVillainous();
    }

    public static void DisableClickables()
    {
        Instance.ClickablesEnabled = false;
    }

    public static void EnableClickables()
    {
        Instance.ClickablesEnabled = true;
    }

    public static void SetOfferWindowButtonDown(string buttonName)
    {
        Instance.offerWindowController.SetButtonDown(buttonName);
    }

    public static void SetCommsWindowButtonDown(string buttonName)
    {
        Instance.commsWindowController.SetButtonDown(buttonName);
    }

    public static void SetDemandWindowButtonDown(string buttonName)
    {
        Instance.demandWindowController.SetButtonDown(buttonName);
    }

    public static void MoveWindowOut()
    {
        EnableClickables();
        Instance.offerWindowController.Mover.TargetPosition = new Vector3(-25f, 0f, 0f);
    }
}

