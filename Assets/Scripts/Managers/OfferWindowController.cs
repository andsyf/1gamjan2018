﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OfferWindowController : MonoBehaviour {

    public GameObject WindowElements;
    public GameObject CommsWindowElements;
    public GameObject DemandWindowElements;
    public GameObject YesButton, NoButton, YesButtonDisabled, YesButtonDown, NoButtonDown;
    public GameObject OffererPosition;
    public ConstantMoveToTarget Mover { get { return mover; } set { mover = value; } }

     [HideInInspector]
    public List<GameObject> Requirements = new List<GameObject>();
    [HideInInspector]
    public List<GameObject> Payment = new List<GameObject>();
    
    SpriteRenderer offererSprite;
    SpriteRenderer yesButtonRend, noButtonRend, yesButtonDownRend, noButtonDownRend;
    BoxCollider2D yesButtonCollider, noButtonCollider;
    MobilePosition mobilePosition;
    ConstantMoveToTarget mover;
    bool queueEnableButtons = false;

    void Awake()
    {
        offererSprite = OffererPosition.GetComponent<SpriteRenderer>();
        yesButtonRend = YesButton.GetComponent<SpriteRenderer>();
        noButtonRend = NoButton.GetComponent<SpriteRenderer>();
        yesButtonDownRend = YesButtonDown.GetComponent<SpriteRenderer>();
        noButtonDownRend = NoButtonDown.GetComponent<SpriteRenderer>();
        mover = GetComponent<ConstantMoveToTarget>();
    }

	// Use this for initialization
	void Start () {
        mobilePosition = gameObject.GetComponent<MobilePosition>();
        yesButtonCollider = YesButton.GetComponent<BoxCollider2D>();
        noButtonCollider = NoButton.GetComponent<BoxCollider2D>();
        yesButtonCollider.enabled = false;
        noButtonCollider.enabled = false;
        YesButtonDisabled.SetActive(false);
        WindowElements.SetActive(false);
        mover.TargetPosition = new Vector3(-25f, 0f, 0f);
    }
	
	// Update is called once per frame
	void Update () {
        if(queueEnableButtons)
        {
            if(mobilePosition.position == mover.TargetPosition)
            {
                EnableButtons();
                queueEnableButtons = false;
            }
        }
        //Reset the buttons to be up, late update input function will reset it if necessary
        yesButtonRend.enabled = noButtonRend.enabled = true;
        yesButtonDownRend.enabled = noButtonDownRend.enabled = false;
    }

    public void SetButtonDown(string buttonName)
    {
        switch(buttonName)
        {
            case "Yes":
                yesButtonDownRend.enabled = true;
                yesButtonRend.enabled = false;
                break;
            case "No":
                noButtonDownRend.enabled = true;
                noButtonRend.enabled = false;
                break;
            default:
                break;
        }
    }

    public void HideNoButton()
    {
        NoButton.SetActive(false);
    }

    public void MoveIn(VillainController vc)
    {
        WindowElements.SetActive(true);
        CommsWindowElements.SetActive(false);
        DemandWindowElements.SetActive(false);
        offererSprite.sprite = vc.GetComponent<SpriteRenderer>().sprite;
        GameDataManager.DisableClickables();
        mover.TargetPosition = Vector3.zero;
        NoButton.SetActive(true);
        if(PlayerProcessor.CanSatisfyContract())
        {
            YesButton.SetActive(true);
        }
        else
        {
            YesButton.SetActive(false);
        }
        YesButtonDisabled.SetActive(!YesButton.activeInHierarchy);

        //Assign the positions of requirements and payment
        float xBase = -1f;
        float reqInc = 2f / (float)Requirements.Count;
        float payInc = 2f / (float)Payment.Count;
        if (reqInc > 2f / 6f) { reqInc = 2f / 6f; }
        if (payInc > 2f / 6f) { payInc = 2f / 6f; }
        for (int i = 0; i < Requirements.Count; i++) {
            Requirements[i].transform.localPosition = new Vector3(xBase + i * reqInc, 0.4f, 0f);
            Requirements[i].GetComponent<SpriteRenderer>().sortingOrder = 5 + i;
        }
        for (int i = 0; i < Payment.Count; i++) {
            Payment[i].transform.localPosition = new Vector3(xBase + i * payInc,-0.8f, 0f);
            Payment[i].GetComponent<SpriteRenderer>().sortingOrder = 5 + i;
        }
        queueEnableButtons = true;
    }

    public void MoveOut()
    {
        DisableButtons();
    }

    public void Clear()
    {
        for(int i = 0; i < Requirements.Count; i++)
        {
            Destroy(Requirements[i]);
        }
        for (int i = 0; i < Payment.Count; i++)
        {
            Destroy(Payment[i]);
        }
        Requirements = new List<GameObject>();
        Payment = new List<GameObject>();
    }

    public void AddRequirement(GameObject g)
    {
        Requirements.Add(g);
        g.transform.parent = gameObject.transform;
    }

    public void AddPayment(GameObject g)
    {
        Payment.Add(g);
        g.transform.parent = gameObject.transform;
    }

    public void EnableButtons()
    {
        noButtonCollider.enabled = true;
        if(PlayerProcessor.CanSatisfyContract())
        {
            Debug.Log("OFFERWINDOWCONTROLLER - This contract can be satisfied");
            YesButton.SetActive(true);
            yesButtonCollider.enabled = true;
            YesButtonDisabled.SetActive(false);
        }
    }

    public void DisableButtons()
    {
        yesButtonCollider.enabled = noButtonCollider.enabled = false;
    }
}
 