﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstantMoveToTarget : MonoBehaviour {

    MobilePosition mobilePosition;
    public Vector3 TargetPosition;

	// Use this for initialization
	void Start () {
        mobilePosition = GetComponent<MobilePosition>();
    }
	
	// Update is called once per frame
	void Update () {
		if(mobilePosition.position != TargetPosition)
        {
            Vector3 vecToTarget = TargetPosition - mobilePosition.position;
            mobilePosition.position += vecToTarget.normalized * mobilePosition.Stats.MovementSpeed * Time.deltaTime;
            Vector3 newVecToTarget = TargetPosition - mobilePosition.position;
            if (newVecToTarget.sqrMagnitude > vecToTarget.sqrMagnitude)
            {
                //We've arrived
                mobilePosition.position = TargetPosition;
            }
        }
	}
}
