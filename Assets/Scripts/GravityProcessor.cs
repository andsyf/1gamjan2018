﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityProcessor : MonoBehaviour {
    MobilePosition mobilePosition;
    float downwardsVelocity = 0f;
    const float downwardsVelocityMax = 2f;

	// Use this for initialization
	void Start () {
        mobilePosition = gameObject.GetComponent<MobilePosition>();
        if (mobilePosition == null)
        {
            Debug.LogError("ERROR - GravityProcessor.cs is trying to get a MobilePosition from an object that doesn't have one");
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (mobilePosition.h > 0f)
        {
            downwardsVelocity += 1f * Time.deltaTime;
            if(downwardsVelocity > downwardsVelocityMax)
            {
                downwardsVelocity = downwardsVelocityMax;
            }
            mobilePosition.h -= downwardsVelocity * Time.deltaTime;
        }
        if(mobilePosition.h < 0f)
        {
            downwardsVelocity = 0f;
            mobilePosition.h = 0f;
        }
    }
}
