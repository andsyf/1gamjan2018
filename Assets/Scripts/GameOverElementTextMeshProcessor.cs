﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverElementTextMeshProcessor : MonoBehaviour {

    string loadedText;
    int textOrdinal;
    float characterAdditionTimer;
    float characterAdditionTimerLimit;
    bool displaying = false;
    [HideInInspector]
    public string DisplayText { get { return loadedText; } set { loadedText = value; textOrdinal = 0; characterAdditionTimer = 0f; characterAdditionTimerLimit = 0.1f; } }
    public TextMesh Speech;
    public TextMesh SpeechOV;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if(gameObject.transform.position.y >= 0f)
        {
            displaying = true;
        }
        if (displaying && loadedText != "" && (textOrdinal < loadedText.Length))
        {
            characterAdditionTimer += Time.deltaTime;
            if (characterAdditionTimer >= characterAdditionTimerLimit)
            {
                characterAdditionTimer -= characterAdditionTimerLimit;
                textOrdinal += 1;
                Speech.text = loadedText.Substring(0, textOrdinal);
                SpeechOV.text = loadedText.Substring(0, textOrdinal);
            }
        }
    }

    public void StartDisplayingText()
    {
        displaying = true;
    }
}
