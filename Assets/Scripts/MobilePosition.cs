﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobilePosition : MonoBehaviour {

    public MobileStats Stats;
    public bool ForceLayerSortOrder = true;
    SpriteRenderer objectRenderer;
    public Vector3 position { get { return new Vector3( x, y, gameObject.transform.position.z); } set { x = value.x; y = value.y; } }
    public float x, y, h;
    float orthographicAdjustmentFactor = 1f / 32f;
    
	// Use this for initialization
	void Start () {
        //Handle being added to an already existing game item
        if (ForceLayerSortOrder)
        {
            objectRenderer = gameObject.GetComponent<SpriteRenderer>();
        }
        position = gameObject.transform.position;
        gameObject.transform.position = new Vector3(x, y + h, gameObject.transform.position.z);
    }
	
	// Update is called once per frame
	void Update () {
        //Convert logical position into physical position
        Vector3 adjustedPosition = new Vector3(x, y + h, gameObject.transform.position.z);
        //Fix the pixel boundaries
        adjustedPosition.x -= (adjustedPosition.x % orthographicAdjustmentFactor);
        adjustedPosition.y -= (adjustedPosition.y % orthographicAdjustmentFactor);
        gameObject.transform.position = adjustedPosition;
        if (ForceLayerSortOrder)
        {
            objectRenderer.sortingOrder = 10000 - (int)(y * 100f);
        }
	}
}
