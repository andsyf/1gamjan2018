﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShakeHorizontally : MonoBehaviour {

    public AnimationCurve shakeCurve;
    float shake = 1f / 32f * 8f;
    float time, maxTime;

    // Use this for initialization
    void Start () {
		if(gameObject.GetComponent<MobilePosition>() == null)
        {
            //No mobile position, turn ourselves off
            Component.Destroy(this);
        }
        maxTime = 1.5f;
	}
	
	// Update is called once per frame
	void LateUpdate () {

        gameObject.transform.position = new Vector3(gameObject.transform.position.x + shake * shakeCurve.Evaluate(time/ maxTime), gameObject.transform.position.y, gameObject.transform.position.z);
        time += Time.deltaTime;
        if (time > maxTime)
        {
            time -= maxTime;
        }
            /*
            gameObject.transform.position = new Vector3(gameObject.transform.position.x + shake, gameObject.transform.position.y, gameObject.transform.position.z);
            time += Time.deltaTime;

            if (time > maxTime)
            {
                shake *= -1f;
                time -= maxTime;
                maxTime += 0.05f;
                if(maxTime > 0.6f)
                {
                    maxTime = 0.1f;
                }
            }*/
        }
}
