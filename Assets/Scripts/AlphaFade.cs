﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlphaFade : MonoBehaviour {

    public AnimationCurve FadeCurve;
    public float CycleTime;
    float currentTime;
    SpriteRenderer Renderer;

	// Use this for initialization
	void Start () {
        currentTime = 0f;
        if(CycleTime == 0f)
        {
            CycleTime = 1f;
        }
        Renderer = gameObject.GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
        float alpha = FadeCurve.Evaluate(currentTime / CycleTime);
        Renderer.color = new Color(Renderer.color.r, Renderer.color.g, Renderer.color.b, alpha);
        currentTime += Time.deltaTime;
        if(currentTime > CycleTime) { currentTime -= CycleTime; }
	}
}
