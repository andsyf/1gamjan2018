﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatUpThenDisappear : MonoBehaviour {

    SpriteRenderer spriteRenderer;
    MobilePosition mobilePosition;
    float timeToDisappear = 2f;

    void Awake()
    {
        mobilePosition = GetComponent<MobilePosition>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        timeToDisappear -= Time.deltaTime;
        if(timeToDisappear < 0f) { timeToDisappear = 0f; }
        Color c = spriteRenderer.color;
        c.a = timeToDisappear;
        spriteRenderer.color = c;
        mobilePosition.position += Vector3.up * mobilePosition.Stats.MovementSpeed * Time.deltaTime;
        if(timeToDisappear == 0f)
        {
            Destroy(gameObject);
        }
	}
}
