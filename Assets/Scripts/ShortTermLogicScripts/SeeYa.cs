﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeeYa : MonoBehaviour {

    public float TimeToLive;

    void Awake()
    {
        TimeToLive = 1f;
    }

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        TimeToLive -= Time.deltaTime;
        if(TimeToLive <= 0f)
        {
            Destroy(gameObject);
        }
	}
}
