﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VentureForth : MonoBehaviour {

    Vector3 door, endPoint, targetPosition;
    MobilePosition mobilePosition;

	// Use this for initialization
	void Start () {
        door = GameDataManager.ConvertedDoorPosition;
        endPoint = GameDataManager.ConvertedVillainSpawnPoint;
        targetPosition = door;
        mobilePosition = gameObject.GetComponent<MobilePosition>();
    }
	
	// Update is called once per frame
	void Update () {
        if (targetPosition != mobilePosition.position)
        {
            Vector3 vecToTarget = targetPosition - mobilePosition.position;
            mobilePosition.position += vecToTarget.normalized * mobilePosition.Stats.MovementSpeed * Time.deltaTime;
            Vector3 newVecToTarget = targetPosition - mobilePosition.position;
            if (newVecToTarget.sqrMagnitude > vecToTarget.sqrMagnitude)
            {
                //We've arrived
                if(targetPosition == door)
                {
                    //This is the door, so go to the endpoint
                    mobilePosition.position = targetPosition;
                    targetPosition = endPoint;
                }
                else
                {
                    //This is the endpoint, deactivate yourself
                    mobilePosition.position = targetPosition;
                }
            }
        }
    }
}
