﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "1GAMJan/Create Mobile Stats")]
public class MobileStats : ScriptableObject {

    public bool isVillain;
    public float MovementSpeed;
    public float MovementSpeedQuick;
    public int GoldPerDay;
    public int MeatPerDay;
    public int BonesPerDay;
    public int MagicPerDay;
}
