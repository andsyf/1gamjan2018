﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FactionState
{
    public List<Message> Introduction;
    public List<Message> OutcomeAccept;
    public List<Message> OutcomeRefuse;
    public FactionState NextStateAccept;
    public FactionState NextStateRefuse;

    public FactionState()
    {
        Introduction = new List<Message>();
        OutcomeAccept = new List<Message>();
        OutcomeRefuse = new List<Message>();
    }
}

[CreateAssetMenu(menuName = "1GAMJan/Create Faction Data")]
public class FactionData : ScriptableObject
{
    public int Score;
    public GameObject CompletionReward;
    public GameObject FactionUnit;
    public Faction EnemyFaction;
    public Faction AllyFaction;
    public List<FactionState> FactionStates;
    public FactionState InitialState;
}