﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectibleClickable : MonoBehaviour {

    [HideInInspector]
    public ResourceType TypeOfClickedResource;

    bool clicked;
    BoxCollider2D boxCollider;
    MobilePosition mobilePosition;
            
    // Use this for initialization
	void Start () {
        clicked = false;
        boxCollider = gameObject.GetComponent<BoxCollider2D>();
        mobilePosition = gameObject.GetComponent<MobilePosition>();

    }
	
	// Update is called once per frame
	void Update () {
        if (!clicked)
        {
            boxCollider.enabled = GameDataManager.Instance.ClickablesEnabled;
        }
    }

    public void Click()
    {
        //Disable the collider so we can't be clicked anymore, start the coroutine
        clicked = true;
        boxCollider.enabled = false;
        StartCoroutine(_Click());
    }

    IEnumerator _Click()
    {
        Vector3 originv3 = mobilePosition.position;
        //Where are we going?
        Vector3 targetv3 = Vector3.zero;
        float curTime = 0f;
        switch (TypeOfClickedResource)
        {
            case ResourceType.Gold:
                targetv3 = GameDataManager.Instance.GoldUIIcon.transform.position;
                break;
            case ResourceType.Bones:
                targetv3 = GameDataManager.Instance.BonesUIIcon.transform.position;
                break;
            case ResourceType.Magic:
                targetv3 = GameDataManager.Instance.MagicUIIcon.transform.position;
                break;
            case ResourceType.Meat:
                targetv3 = GameDataManager.Instance.MeatUIIcon.transform.position;
                break;
            default:
                break;
        }
        //Boost the scale of the object, then move to the correct collection point, getting smaller as we go
        gameObject.transform.localScale *= 2f;
        float scalar = gameObject.transform.localScale.x;
        while (curTime < 1f)
        {
            float calculatedScalar = (scalar - 1f) * (1f - curTime) + 1f;
            gameObject.transform.localScale = new Vector3(calculatedScalar, calculatedScalar, calculatedScalar);
            if (curTime > 0.3f)
            {
                float completionFactor = (curTime - 0.3f) / (1f - 0.3f);
                mobilePosition.position = originv3 * (1f - completionFactor) + targetv3 * completionFactor;
            }
            yield return 0;
            curTime += Time.deltaTime;
        }
        //Add resources
        PlayerProcessor.IncrementResource(TypeOfClickedResource);
    }
}
