﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkAimlesslyOutside : MonoBehaviour
{

    bool running;
    float waitTime;
    Vector3 targetPosition;
    MobilePosition mobilePosition;

    // Use this for initialization
    void Start()
    {
        waitTime = Random.Range(2f, 4f);
        mobilePosition = gameObject.GetComponent<MobilePosition>();
        if (mobilePosition == null)
        {
            Debug.LogError("ERROR - WalkAimlesslyOutside.cs is trying to get a MobilePosition from an object that doesn't have one");
        }
        running = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (!running)
        {
            return;
        }
        if (waitTime > 0f)
        {
            waitTime -= Time.deltaTime;
        }
        else
        {
            waitTime = Random.Range(5f, 7f);
            Vector3 absolute = new Vector3((float)Random.Range(-5, 5), (float)Random.Range(1, 8), 0f);
            targetPosition = absolute;
        }
        if (targetPosition != mobilePosition.position)
        {
            Vector3 vecToTarget = targetPosition - mobilePosition.position;
            mobilePosition.position += vecToTarget.normalized * mobilePosition.Stats.MovementSpeed * Time.deltaTime;
            Vector3 newVecToTarget = targetPosition - mobilePosition.position;
            if (newVecToTarget.sqrMagnitude > vecToTarget.sqrMagnitude)
            {
                //We've arrived
                mobilePosition.position = targetPosition;
            }
        }
    }

    void StopAI()
    {
        running = false;
    }

    void StartAI()
    {
        running = true;
    }
}
