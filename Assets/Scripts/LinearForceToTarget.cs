﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinearForceToTarget : MonoBehaviour {

    MobilePosition mobilePosition;
    Vector3 targetv3;
    Vector3 originv3;
    Vector3 vectorToTargetv3;
    float curTime = 0f;
    float moveTime = 0.5f;

    public Vector2 target;
    AnimationCurve distanceToTarget;

    // Use this for initialization
    void Start()
    {
        mobilePosition = gameObject.GetComponent<MobilePosition>();
        if (mobilePosition == null)
        {
            Debug.LogError("ERROR - LinearForce.cs is trying to get a MobilePosition from an object that doesn't have one");
        }
        targetv3 = new Vector3(target.x, target.y, 0f);
        originv3 = gameObject.transform.position;
        vectorToTargetv3 = targetv3 - originv3;
        vectorToTargetv3 = vectorToTargetv3.normalized;
        distanceToTarget = GameDataManager.Instance.LinearForceToTargetCurve;
        moveTime += Random.Range(-0.2f, 0.2f);
    }

    // Update is called once per frame
    void Update()
    {
        if (curTime < moveTime)
        {
            float evaluatedValue = distanceToTarget.Evaluate(curTime / moveTime);
            mobilePosition.position = originv3 * (1f - evaluatedValue) + targetv3 * evaluatedValue;
            curTime += Time.deltaTime;
        }
        else
        {
            CollectibleClickable cc = gameObject.AddComponent<CollectibleClickable>();
            switch(gameObject.name)
            {
                case "Bones":
                    Debug.Log("Assigning bones");
                    cc.TypeOfClickedResource = ResourceType.Bones;
                    break;
                case "GoldBag":
                    Debug.Log("Assigning gold");
                    cc.TypeOfClickedResource = ResourceType.Gold;
                    break;
                case "Magic":
                    Debug.Log("Assigning magic");
                    cc.TypeOfClickedResource = ResourceType.Magic;
                    break;
                case "Meat":
                    Debug.Log("Assigning meat");
                    cc.TypeOfClickedResource = ResourceType.Meat;
                    break;
                default:
                    break;
            }
            Destroy(this);
        }
    }
}
